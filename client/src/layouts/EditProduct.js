import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import { connect } from "react-redux";
import { updateProduct, resetSaved } from "../actions/product_actions";

//materiel Ui
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import FormControl from "@material-ui/core/FormControl";
import { makeStyles } from "@material-ui/core/styles";
import CustomSidebar from "components/Sidebar/CustomSidebar";
import { FormGroup } from "@material-ui/core";
import { Formik } from "formik";
import Alert from "@material-ui/lab/Alert";
import Collapse from "@material-ui/core/Collapse";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginLeft: theme.spacing(12.5),
      paddingTop: theme.spacing(5),
      width: theme.spacing(189.6),
      height: "89.8vh",
      borderRadius: 40,
      border: "1px solid #dadce0",
    },
  },
  root1: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(3, 15, 0),
      width: theme.spacing(200),
      height: theme.spacing(88),
      paddingLeft: theme.spacing(20),
      paddingTop: theme.spacing(5),
      paddingBottom: theme.spacing(4),
      borderRadius: 20,
      border: "1px solid #dadce0",
    },
  },
  title: {
    textAlign: "center",
    color: "gray",
  },
  form: {
    width: "100%",
  },
  submit: {
    height: 50,
    width: 300,
    fontSize: 20,
    color: "SLATEBLUE",
    backgroundColor: "white",
    borderRadius: 30,
    border: "1px solid #dadce0",
  },
  textfild: {
    width: 420,
  },
  alerts: {
    height: 45,
    marginLeft: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  divStyle: {
    marginBottom: theme.spacing(0),
    display: "flex",
    flexDirection: "row",
    height: 68,
  },
}));

const defaultProps = {
  bgcolor: "SLATEBLUE",
  border: "1px solid #dadce0",
  borderRadius: 40,
  style: {
    width: "90vw",
    height: "90vh",
    spacing: 0,
    margin: 0,
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
};

const EditProductComponent = (props) => {
  // styles
  const classNamees = useStyles();
  const inputNames = {
    color: "gray",
    fontSize: 18,
  };
  //states
  var [open, setOpen] = useState(false);
  const [file, setFile] = useState("");
  const [filename, setFilename] = useState("Choose File");

  let item;
  try {
    item = props.location.state.item;
  } catch (e) {
    item = undefined;
  }
  if (!item) props.history.push("/myProducts");

  const onChange = (e) => {
    setFile(e.target.files[0]);
    setFilename(e.target.files[0].name);
  };

  useEffect(() => {
    const { updated, resetSaved } = props;
    if (updated) {
      resetSaved();
      setTimeout(() => {
        setOpen(true);
      }, 3000);
      props.history.push("/myProducts");
    }
  });

  return (
    <Box {...defaultProps}>
      <div style={{ display: "flex", flexDirection: "rows" }}>
        <CustomSidebar />
        <div style={{ display: "flex", flexDirection: "column" }}>
          <div className={classNamees.root}>
            <Paper>
              <div className={classNamees.title}>
                <Typography component="h1" variant="h3">
                  Edit Product
                </Typography>
              </div>
              <div className={classNamees.root1}>
                <Paper>
                  <Formik
                    initialValues={{
                      productName: item.productName,
                      productDescription: item.productDescription,
                      productCategory: item.productCategory,
                      productTags: item.productTags,
                      productImage: item.productImage,
                      productInitialPrice: item.productInitialPrice,
                      productOnSalePrice: item.productOnSalePrice,
                      productVisibility: item.productVisibility,
                    }}
                    onSubmit={(values) => {
                      const item = props.location.state.item;
                      const data = new FormData();

                      data._id = item._id;

                      data.append("productName", values.productName);
                      data.append(
                        "productDescription",
                        values.productDescription
                      );
                      data.append("productCategory", values.productCategory);
                      data.append("productTags", values.productTags);
                      data.append("productImage", file);
                      data.append(
                        "productInitialPrice",
                        values.productInitialPrice
                      );
                      data.append(
                        "productOnSalePrice",
                        values.productOnSalePrice
                      );
                      data.append(
                        "productVisibility",
                        values.productVisibility
                      );

                      props.updateProduct(data);
                    }}
                    validationSchema={Yup.object().shape({
                      productName: Yup.string().required(),
                      productDescription: Yup.string().required(),
                      productCategory: Yup.string().required(),
                      productTags: Yup.string().required(),
                      productInitialPrice: Yup.number()
                        .required()
                        .min(1),
                      productOnSalePrice: Yup.number()
                        .required()
                        .min(1),
                      productVisibility: Yup.string().required(),
                    })}
                  >
                    {({
                      values,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      errors,
                      touched,
                      isValid,
                      isSubmitting,
                    }) => (
                      <div>
                        <FormGroup>
                          <div className={classNamees.divStyle}>
                            <label
                              className="col-sm-3 col-form-label"
                              style={inputNames}
                            >
                              Product Name
                            </label>
                            <TextField
                              className={classNamees.textfild}
                              id="productName"
                              name="productName"
                              value={values.productName}
                              variant="outlined"
                              size="small"
                              required
                              label="Product Name"
                              autoComplete="productName"
                              onChange={handleChange}
                              autoFocus
                              onBlur={handleBlur}
                            />
                            {errors.productName && touched.productName ? (
                              <Alert
                                severity="error"
                                className={classNamees.alerts}
                              >
                                {errors.productName}
                              </Alert>
                            ) : null}
                          </div>
                          <div className={classNamees.divStyle}>
                            <label
                              className="col-sm-3 col-form-label"
                              style={inputNames}
                            >
                              Product Description
                            </label>
                            <TextField
                              className={classNamees.textfild}
                              id="productDescription"
                              name="productDescription"
                              value={values.productDescription}
                              variant="outlined"
                              size="small"
                              required
                              label="Product Description"
                              autoComplete="productDescription"
                              onChange={handleChange}
                              multiline
                              rows={1}
                              rowsMax={4}
                              onBlur={handleBlur}
                            />
                            {errors.productDescription &&
                            touched.productDescription ? (
                              <Alert
                                severity="error"
                                className={classNamees.alerts}
                              >
                                {errors.productDescription}
                              </Alert>
                            ) : null}
                          </div>
                          <br />
                          <div className={classNamees.divStyle}>
                            <label
                              className="col-sm-3 col-form-label"
                              style={inputNames}
                            >
                              Product Gategory
                            </label>
                            <TextField
                              className={classNamees.textfild}
                              id="productCategory"
                              name="productCategory"
                              value={values.productCategory}
                              variant="outlined"
                              size="small"
                              required
                              label=" Product Category"
                              autoComplete="productCategory"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            {errors.productCategory &&
                            touched.productCategory ? (
                              <Alert
                                severity="error"
                                className={classNamees.alerts}
                              >
                                {errors.productCategory}
                              </Alert>
                            ) : null}
                          </div>
                          <div className={classNamees.divStyle}>
                            <label
                              className="col-sm-3 col-form-label"
                              style={inputNames}
                            >
                              Product Tags
                            </label>
                            <TextField
                              className={classNamees.textfild}
                              id="productTags"
                              name="productTags"
                              value={values.productTags}
                              variant="outlined"
                              size="small"
                              required
                              label=" Product Tags"
                              autoComplete="productTags"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            {errors.productTags && touched.productTags ? (
                              <Alert
                                severity="error"
                                className={classNamees.alerts}
                              >
                                {errors.productTags}
                              </Alert>
                            ) : null}
                          </div>
                          <div className={classNamees.divStyle}>
                            <label
                              className="col-sm-3 col-form-label"
                              style={inputNames}
                            >
                              Product Image Gallery
                            </label>
                            <div
                              className="custom-file mb-4"
                              style={{ width: 420 }}
                            >
                              <input
                                type="file"
                                className="custom-file-input"
                                onChange={onChange}
                                id="productImage"
                                accept="image/*"
                              />
                              <label
                                className="custom-file-label"
                                htmlFor="productImage"
                              >
                                {filename === "Choose File"
                                  ? item.productImage.split("uploads")
                                  : filename}
                              </label>
                            </div>
                          </div>
                          <div className={classNamees.divStyle}>
                            <label
                              className="col-sm-3 col-form-label"
                              style={inputNames}
                            >
                              Product Initial Price
                            </label>
                            <TextField
                              className={classNamees.textfild}
                              id="productInitialPrice"
                              name="productInitialPrice"
                              value={values.productInitialPrice}
                              variant="outlined"
                              size="small"
                              required
                              label=" Product initial price"
                              autoComplete="productInitialPrice"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            {errors.productInitialPrice &&
                            touched.productInitialPrice ? (
                              <Alert
                                severity="error"
                                className={classNamees.alerts}
                              >
                                product Intiale Price must be a `number` type
                              </Alert>
                            ) : null}
                          </div>
                          <div className={classNamees.divStyle}>
                            <label
                              className="col-sm-3 col-form-label"
                              style={inputNames}
                            >
                              Product On Sale Price
                            </label>
                            <TextField
                              className={classNamees.textfild}
                              id="productOnSalePrice"
                              name="productOnSalePrice"
                              value={values.productOnSalePrice}
                              variant="outlined"
                              size="small"
                              required
                              label=" Product on sale price"
                              autoComplete="productOnSalePrice"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            {errors.productOnSalePrice &&
                            touched.productOnSalePrice ? (
                              <Alert
                                severity="error"
                                className={classNamees.alerts}
                              >
                                product On Sale Price must be a `number` type
                              </Alert>
                            ) : null}
                          </div>
                          <div className={classNamees.divStyle}>
                            <label
                              className="col-sm-3 col-form-label"
                              style={inputNames}
                            >
                              Product Visibility
                            </label>
                            <FormControl component="fieldset">
                              <RadioGroup
                                row
                                aria-label="PRODUCT VISIBILITY "
                                id="productVisibility"
                                name="productVisibility"
                                value={values.productVisibility}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <FormControlLabel
                                  value="Published"
                                  control={<Radio />}
                                  label="Published"
                                  style={{
                                    marginRight: 100,
                                    marginLeft: 20,
                                    color: "DIMGRAY",
                                  }}
                                />
                                <FormControlLabel
                                  value=" NotPublished"
                                  control={<Radio />}
                                  label=" Not Published"
                                  style={{ color: "DIMGRAY" }}
                                />
                              </RadioGroup>
                            </FormControl>
                            {errors.productVisibility ? (
                              <Alert
                                severity="error"
                                className={classNamees.alerts}
                              >
                                {errors.productVisibility}
                              </Alert>
                            ) : null}
                          </div>
                        </FormGroup>
                        <div
                          style={{
                            display: "flex",
                            flexWrap: "wrap",
                            flexDirection: "column",
                            marginLeft: 350,
                          }}
                        >
                          <Button
                            type="submit"
                            variant="contained"
                            className={classNamees.submit}
                            onClick={handleSubmit}
                            disabled={!isValid || isSubmitting}
                          >
                            Update Product
                          </Button>
                        </div>
                        <Collapse
                          in={open}
                          style={{
                            width: 500,
                            marginLeft: "23%",
                            marginTop: 20,
                            borderRadius: 20,
                          }}
                        >
                          <Alert
                            style={{
                              borderRadius: 20,
                            }}
                            action={
                              isSubmitting
                                ? setTimeout(() => {
                                    open = true;
                                  }, 1500)
                                : setOpen(false)
                            }
                          >
                            Product updated successfully
                          </Alert>
                        </Collapse>
                      </div>
                    )}
                  </Formik>
                </Paper>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    </Box>
  );
};

const mapStateToProps = ({ product }) => {
  return {
    updated: product.updated,
  };
};

const EditProduct = connect(
  mapStateToProps,
  { updateProduct, resetSaved }
)(EditProductComponent);
export { EditProduct };
