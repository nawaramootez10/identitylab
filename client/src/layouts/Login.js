import React, { useEffect } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { signIn } from "../actions";

//materiel Ui
import { FormHelperText, FormGroup, Link } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardIcon from "components/Card/CardIcon.js";
import PermIdentityOutlinedIcon from "@material-ui/icons/PermIdentityOutlined";
import loginImg from "assets/img/loginImg.JPG";
import { Alert, AlertTitle } from "@material-ui/lab";
import ErrorIcon from "@material-ui/icons/Error";

//password field
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <b>Identity.com </b>
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  title: {
    width: "100%",
    margin: theme.spacing(3, 0, 5),
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    position: "fixed",
  },
  submit: {
    margin: theme.spacing(8, 0, 2),
    height: 50,
    fontSize: 15,
    color: "white",
    backgroundColor: "SLATEBLUE",
    borderRadius: 20,
    border: "1px solid #dadce0",
  },
  root: {
    width: "75vw",
    height: "80vh",
    spacing: 0,
    margin: "0 auto",
    borderRadius: 10,
    border: "1px solid #dadce0",
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
}));

const LoginPage = (props) => {
  // styles
  const classNamees = useStyles();

  //states and function
  const [values, setValues] = React.useState({
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const renderErrorIfAny = () => {
    const { error } = props;
    if (error) {
      return (
        <Alert severity="error">
          <AlertTitle>Failed to login</AlertTitle>
          {error}
        </Alert>
      );
    }
  };

  useEffect(() => {
    const { isAuth } = props;

    if (isAuth) {
      props.history.push("/home");
    }
  });

  const { showPassword } = values;

  return (
    <Card className={classNamees.root}>
      <CardHeader color="primary" stats icon>
        <CardIcon color="primary">
          <PermIdentityOutlinedIcon />
        </CardIcon>
      </CardHeader>
      <CardBody>
        <div className="row">
          <div
            className="col-lg-5"
            style={{ paddingLeft: 50, paddingRight: 50 }}
          >
            <div className={classNamees.title}>
              <Typography component="h1" variant="h4">
                Sign in
              </Typography>
              <Typography
                component="h3"
                variant="subtitle2"
                style={{ color: "grey" }}
              >
                Connect with your account
              </Typography>
            </div>
            {renderErrorIfAny()}
            <br></br>
            <Formik
              initialValues={{ email: "", password: "" }}
              onSubmit={(values) => {
                props.signIn(values);
              }}
              validationSchema={Yup.object().shape({
                email: Yup.string()
                  .email()
                  .required(),
                password: Yup.string()
                  .min(6)
                  .required(),
              })}
            >
              {({
                values,
                handleChange,
                handleSubmit,
                handleBlur,
                isValid,
                errors,
                touched,
              }) => (
                <div>
                  <FormGroup>
                    {errors.email && touched.email ? (
                      <div>
                        <TextField
                          id="email"
                          name="email"
                          value={values.email}
                          error
                          fullWidth
                          label="Email Address"
                          type="email"
                          variant="outlined"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          autoFocus
                        />
                        <FormHelperText error>
                          <ErrorIcon color="error" fontSize="small" />
                          &nbsp; {errors.email}
                        </FormHelperText>
                      </div>
                    ) : (
                      <TextField
                        id="email"
                        name="email"
                        value={values.email}
                        variant="outlined"
                        required
                        fullWidth
                        label="Email Address"
                        type="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        autoFocus
                      />
                    )}
                    <br></br>
                    {errors.password && touched.password ? (
                      <div>
                        <FormControl fullWidth variant="outlined">
                          <InputLabel htmlFor="password" required>
                            Password
                          </InputLabel>
                          <OutlinedInput
                            error
                            id="password"
                            name="password"
                            value={values.password}
                            type={showPassword ? "text" : "password"}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={handleClickShowPassword}
                                  onMouseDown={handleMouseDownPassword}
                                  edge="end"
                                >
                                  {showPassword ? (
                                    <Visibility />
                                  ) : (
                                    <VisibilityOff />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            }
                            labelWidth={80}
                          />
                        </FormControl>
                        <FormHelperText error>
                          <ErrorIcon color="error" fontSize="small" />
                          &nbsp; {errors.password}
                        </FormHelperText>
                      </div>
                    ) : (
                      <FormControl fullWidth variant="outlined">
                        <InputLabel htmlFor="password" required>
                          Password
                        </InputLabel>
                        <OutlinedInput
                          id="password"
                          name="password"
                          value={values.password}
                          type={showPassword ? "text" : "password"}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                                edge="end"
                              >
                                {showPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                          labelWidth={80}
                        />
                      </FormControl>
                    )}
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      className={classNamees.submit}
                      onClick={handleSubmit}
                      disabled={!isValid}
                    >
                      Sign In
                    </Button>
                  </FormGroup>
                </div>
              )}
            </Formik>
            <Grid container>
              <Grid item xs>
                <Link href="/signup" style={{ fontSize: 16 }}>
                  Don't have an account?
                </Link>
              </Grid>
              <Grid item>
                <Link href="/#" style={{ fontSize: 16 }}>
                  Forgot password?
                </Link>
              </Grid>
            </Grid>
            <Box mt={10}>
              <Copyright />
            </Box>
          </div>
          <div className="col-lg-6 d-none d-lg-block bg-login-image">
            <img
              alt="login"
              src={loginImg}
              style={{
                marginTop: -76,
                height: "98.9%",
                marginLeft: 60,
                width: "111.5%",
                borderRadius: 10,
              }}
            />
          </div>
        </div>
      </CardBody>
    </Card>
  );
};

const mapStateToProps = ({ auth }) => {
  return {
    attempting: auth.attempting,
    error: auth.error,
    isAuth: auth.isAuth,
  };
};

const Login = connect(
  mapStateToProps,
  { signIn }
)(LoginPage);
export { Login };
