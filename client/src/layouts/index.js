export * from "./Login";
export * from "./Home";
export * from "./AddCustomer";
export * from "./EditProfil";
export * from "./MapView";
export * from "./AddProduct";
export * from "./PromotProducts";
export * from "./ShowCustomer";
export * from "./Signup";
export * from "./Community";
export * from "./MyProducts";
export * from "./EditProduct";
