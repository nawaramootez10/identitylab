import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import { Typography, Paper } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import CropFreeIcon from "@material-ui/icons/CropFree";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(4, 6, 4),
      width: theme.spacing(50),
      height: theme.spacing(20),
      paddingTop: theme.spacing(2),
      paddingLeft: theme.spacing(2),
      borderRadius: 16,
    },
  },
  root1: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(4, 2, 4),
      width: theme.spacing(50),
      height: theme.spacing(20),
      padding: theme.spacing(2, 4),
      borderRadius: 16,
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 1),
    height: 50,
    width: 250,
    fontSize: 15,
    borderRadius: 40,
  },
}));

const defaultProps = {
  bgcolor: "SLATEBLUE",
  borderColor: "white",
  m: 1,
  border: 1,
  borderRadius: 50,
  style: {
    width: "60rem",
    height: "30rem",
    position: "fixed",
    left: "20%",
    top: "20%",
    zIndex: 999,
    backgroundColor: "#ee",
    display: "flex",
    flexDirection: "column",
  },
};

const AddCustomer = (props) => {
  const classNamees = useStyles();
  let dialog = (
    <Box {...defaultProps}>
      <Typography
        component="h3"
        variant="h3"
        align="center"
        style={{ color: "white", marginTop: 20 }}
      >
        <b> Add New Customer </b>
      </Typography>
      <br></br>
      <Typography
        variant="h4"
        component="h4"
        align="center"
        style={{ color: "GREENYELLOW" }}
      >
        Using
      </Typography>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <div className={classNamees.root}>
          <Paper>
            <form className={classNamees.form} noValidate>
              <div style={{ alignContent: "center" }}>
                <Typography variant="h5" align="center">
                  Customer ID
                </Typography>
              </div>
              <br></br>
              <div className="col-sm-11">
                <TextField
                  id="Customer ID"
                  label="Customer ID"
                  variant="outlined"
                  fullWidth
                />
              </div>
            </form>
          </Paper>
        </div>
        <div className={classNamees.root1}>
          <Paper>
            <form className={classNamees.form} noValidate>
              <div style={{ alignContent: "center" }}>
                <Typography variant="h5" align="center">
                  Customer QR Code
                </Typography>
              </div>
              <br></br>
              <div className="col-sm-15">
                <CropFreeIcon
                  style={{
                    fontSize: 80,
                    width: 130,
                    height: 70,
                    marginLeft: "30%",
                  }}
                  color="action"
                />
              </div>
            </form>
          </Paper>
        </div>
      </div>
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          flexDirection: "column",
          alignContent: "center",
        }}
      >
        <Button
          type="submit"
          variant="contained"
          color="secondary"
          className={classNamees.submit}
          onClick={() => props.onClose}
        >
          Add Customer
        </Button>
      </div>
    </Box>
  );

  if (!props.isOpen) {
    dialog = null;
  }
  return <div>{dialog}</div>;
};

export { AddCustomer };
