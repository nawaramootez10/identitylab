import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchCustomer, deleteCustomer } from "../actions/community_actions";
import { Spinner, CustomerItemList } from "../components";
//Material ui
import Box from "@material-ui/core/Box";
import { Typography, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import CustomSidebar from "components/Sidebar/CustomSidebar";
import { ListGroup } from "reactstrap";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginLeft: theme.spacing(12.5),
      paddingTop: theme.spacing(5),
      width: theme.spacing(189.6),
      height: "89.8vh",
      borderRadius: 40,
      border: "1px solid #dadce0",
    },
  },
  root1: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(5, 25, 0),
      padding: theme.spacing(0, 15, 0),
      width: theme.spacing(150),
      height: theme.spacing(75),
      borderRadius: 50,
      border: "1px solid #dadce0",
    },
  },
  title: {
    textAlign: "center",
    color: "gray",
    margin: theme.spacing(2, 5, 0),
  },
}));

const defaultProps = {
  bgcolor: "SLATEBLUE",
  border: "1px solid #dadce0",
  borderRadius: 40,
  style: {
    width: "90vw",
    height: "90vh",
    spacing: 0,
    margin: 0,
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
};

const ShowCustomerComponent = (props) => {
  const classNamees = useStyles();

  useEffect(() => {
    const { fetchCustomer } = props;
    fetchCustomer();
  }, []);

  const { fetching, community } = props;

  const showSpinner = () => {
    if (fetching) {
      return <Spinner />;
    }
  };

  return (
    <Box {...defaultProps}>
      <div style={{ display: "flex", flexDirection: "rows" }}>
        <CustomSidebar />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <div className={classNamees.root}>
            <Paper>
              <div className={classNamees.title}>
                <Typography
                  component="h5"
                  variant="h2"
                  align="center"
                  style={{ fontSize: 60 }}
                >
                  COMMUNITY
                </Typography>
              </div>
              <div className={classNamees.root1}>
                <Paper>
                  <Typography
                    variant="h3"
                    component="h5"
                    align="center"
                    style={{
                      fontSize: 35,
                      marginTop: 20,
                      color: "MEDIUMBLUE",
                    }}
                  >
                    Customer List
                  </Typography>
                  <hr />
                  {showSpinner()}
                  <ListGroup style={{ overflowY: "auto" }}>
                    {community.map((item) => (
                      <CustomerItemList
                        key={item._id}
                        item={item}
                        onDelete={() => {
                          const { deleteCustomer } = props;
                          deleteCustomer(item._id);
                        }}
                      />
                    ))}
                  </ListGroup>
                </Paper>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    </Box>
  );
};

const mapStateToProps = ({ community }) => {
  return {
    fetching: community.fetching,
    community: community.community,
  };
};

const ShowCustomer = connect(
  mapStateToProps,
  { fetchCustomer, deleteCustomer }
)(ShowCustomerComponent);
export { ShowCustomer };
