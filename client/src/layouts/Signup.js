import React, { useEffect } from "react";
import signupImg from "assets/img/signupImg.PNG";
import { Formik } from "formik";
import * as Yup from "yup";
import { savedUser } from "../actions/register_actions";
import { connect } from "react-redux";
import { ErrorMessage } from "../components";

//material-Ui Core
import { FormGroup, Link, FormHelperText } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import ErrorIcon from "@material-ui/icons/Error";
import { Alert, AlertTitle } from "@material-ui/lab";
import Collapse from "@material-ui/core/Collapse";

//material-Ui Card
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardIcon from "components/Card/CardIcon.js";

//material-Ui Icons
import PermIdentityOutlinedIcon from "@material-ui/icons/PermIdentityOutlined";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import OutlinedInput from "@material-ui/core/OutlinedInput";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <b>Identity.com </b>
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  title: {
    width: "100%",
    margin: theme.spacing(2, 0, 2),
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    position: "fixed",
  },
  submit: {
    margin: theme.spacing(0, 0, 1),
    height: 50,
    fontSize: 15,
    color: "white",
    backgroundColor: "SLATEBLUE",
    borderRadius: 20,
    border: "1px solid #dadce0",
  },
  root: {
    width: "75vw",
    height: "80vh",
    spacing: 0,
    margin: 0,
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    border: "1px solid #dadce0",
  },
}));

const SignupPage = (props) => {
  // styles
  const classNamees = useStyles();

  //states and function
  const [values, setValues] = React.useState({
    showPassword: false,
    showConfirmPassword: false,
  });

  var [open, setOpen] = React.useState(false);

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };
  const handleClickShowRPassword = () => {
    setValues({ ...values, showConfirmPassword: !values.showConfirmPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const handleMouseDownRPassword = (event) => {
    event.preventDefault();
  };

  const renderErrorIfAny = () => {
    const { error } = props;
    if (error) {
      return (
        <Alert severity="error">
          <AlertTitle>Failed to register!</AlertTitle>
          {error}
        </Alert>
      );
    }
  };

  useEffect(() => {
    const { saved } = props;

    if (saved) {
      setTimeout(() => {
        setOpen(true);
      }, 3000);
    }
  });

  const { showPassword, showConfirmPassword } = values;

  return (
    <Card className={classNamees.root}>
      <CardHeader color="primary" stats icon>
        <CardIcon color="primary">
          <PermIdentityOutlinedIcon />
        </CardIcon>
      </CardHeader>
      <CardBody>
        <div className="row">
          <div
            className="col-lg-5"
            style={{ paddingLeft: 50, paddingRight: 50 }}
          >
            <div className={classNamees.title}>
              <Typography component="h1" variant="h4">
                Sign Up
              </Typography>
              <Typography
                component="h3"
                variant="subtitle2"
                style={{ color: "grey" }}
              >
                Create a free account and get started
              </Typography>
            </div>
            {renderErrorIfAny()}
            <br></br>
            <Formik
              initialValues={{
                managerName: "",
                workspaceName: "",
                category: "",
                description: "",
                email: "",
                password: "",
                confirmPassword: "",
              }}
              onSubmit={(values) => {
                props.savedUser(values);
              }}
              validationSchema={Yup.object().shape({
                managerName: Yup.string().required(),
                workspaceName: Yup.string().required(),
                category: Yup.string().required(),
                description: Yup.string()
                  .required()
                  .min(3),
                email: Yup.string()
                  .email()
                  .required(),
                password: Yup.string()
                  .min(6)
                  .required(),
                confirmPassword: Yup.string()
                  .oneOf(
                    [Yup.ref("password")],
                    "Those passwords didn't match. Try again."
                  )
                  .required(),
              })}
            >
              {({
                values,
                handleChange,
                handleSubmit,
                handleBlur,
                handleReset,
                isSubmitting,
                isValid,
                errors,
                touched,
              }) => (
                <div>
                  <ErrorMessage />
                  <FormGroup>
                    <div
                      style={{
                        display: "flex",
                        flexWrap: "wrap",
                        justifyContent: "space-between",
                      }}
                    >
                      {errors.managerName && touched.managerName ? (
                        <div>
                          <TextField
                            id="managerName"
                            name="managerName"
                            variant="outlined"
                            size="small"
                            margin="dense"
                            required
                            error
                            label="Manager Name"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            autoFocus
                          />
                          <FormHelperText error>
                            <ErrorIcon color="error" fontSize="small" />
                            &nbsp; {errors.managerName}
                          </FormHelperText>
                        </div>
                      ) : (
                        <TextField
                          id="managerName"
                          name="managerName"
                          value={values.managerName}
                          variant="outlined"
                          size="small"
                          margin="dense"
                          required
                          label="Manager Name"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          autoFocus
                        />
                      )}
                      {errors.workspaceName && touched.workspaceName ? (
                        <div>
                          <TextField
                            id="workspaceName"
                            name="workspaceName"
                            variant="outlined"
                            size="small"
                            margin="dense"
                            required
                            error
                            label="Workspace Name"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                          <FormHelperText error>
                            <ErrorIcon color="error" fontSize="small" />
                            &nbsp; {errors.workspaceName}
                          </FormHelperText>
                        </div>
                      ) : (
                        <TextField
                          id="workspaceName"
                          name="workspaceName"
                          value={values.workspaceName}
                          variant="outlined"
                          size="small"
                          margin="dense"
                          required
                          label="Workspace Name"
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      )}
                    </div>
                    <div
                      style={{
                        display: "flex",
                        flexWrap: "wrap",
                        justifyContent: "space-between",
                      }}
                    >
                      {errors.category && touched.category ? (
                        <div>
                          <TextField
                            id="category"
                            name="category"
                            variant="outlined"
                            size="small"
                            margin="dense"
                            required
                            error
                            label="Category"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                          <FormHelperText error>
                            <ErrorIcon color="error" fontSize="small" />
                            &nbsp; {errors.category}
                          </FormHelperText>
                        </div>
                      ) : (
                        <TextField
                          id="category"
                          name="category"
                          value={values.category}
                          variant="outlined"
                          size="small"
                          margin="dense"
                          required
                          label="Category"
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      )}
                      &nbsp; &nbsp; &nbsp;
                      {errors.description && touched.description ? (
                        <div>
                          <TextField
                            id="description"
                            name="description"
                            variant="outlined"
                            size="small"
                            margin="dense"
                            required
                            error
                            label="Description"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                          <FormHelperText error>
                            <ErrorIcon color="error" fontSize="small" />
                            &nbsp; {errors.description}
                          </FormHelperText>
                        </div>
                      ) : (
                        <TextField
                          id="description"
                          name="description"
                          value={values.description}
                          variant="outlined"
                          size="small"
                          margin="dense"
                          required
                          label="Description"
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      )}
                    </div>
                    {errors.email && touched.email ? (
                      <div>
                        <TextField
                          id="email"
                          name="email"
                          variant="outlined"
                          size="small"
                          margin="dense"
                          required
                          error
                          fullWidth
                          label="Email Address"
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                        <FormHelperText error>
                          <ErrorIcon color="error" fontSize="small" />
                          &nbsp; {errors.email}
                        </FormHelperText>
                      </div>
                    ) : (
                      <TextField
                        id="email"
                        name="email"
                        value={values.email}
                        variant="outlined"
                        size="small"
                        margin="dense"
                        required
                        fullWidth
                        label="Email Address"
                        type="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    )}
                    {errors.password && touched.password ? (
                      <div>
                        <FormControl
                          fullWidth
                          variant="outlined"
                          size="small"
                          margin="dense"
                        >
                          <InputLabel htmlFor="password" required>
                            Password
                          </InputLabel>
                          <OutlinedInput
                            error
                            id="password"
                            name="password"
                            value={values.password}
                            type={showPassword ? "text" : "password"}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={handleClickShowPassword}
                                  onMouseDown={handleMouseDownPassword}
                                  edge="end"
                                >
                                  {showPassword ? (
                                    <Visibility />
                                  ) : (
                                    <VisibilityOff />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            }
                            labelWidth={80}
                          />
                        </FormControl>
                        <FormHelperText error>
                          <ErrorIcon color="error" fontSize="small" />
                          &nbsp; {errors.password}
                        </FormHelperText>
                      </div>
                    ) : (
                      <FormControl
                        fullWidth
                        variant="outlined"
                        size="small"
                        margin="dense"
                      >
                        <InputLabel htmlFor="password" required>
                          Password
                        </InputLabel>
                        <OutlinedInput
                          id="password"
                          name="password"
                          value={values.password}
                          type={showPassword ? "text" : "password"}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                                edge="end"
                              >
                                {showPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                          labelWidth={80}
                        />
                      </FormControl>
                    )}
                    {errors.confirmPassword && touched.confirmPassword ? (
                      <div>
                        <FormControl
                          fullWidth
                          variant="outlined"
                          size="small"
                          margin="dense"
                        >
                          <InputLabel htmlFor="confirmPassword" required>
                            Confirm Password
                          </InputLabel>
                          <OutlinedInput
                            error
                            id="confirmPassword"
                            name="confirmPassword"
                            value={values.confirmPassword}
                            type={showConfirmPassword ? "text" : "password"}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={handleClickShowRPassword}
                                  onMouseDown={handleMouseDownRPassword}
                                  edge="end"
                                >
                                  {showConfirmPassword ? (
                                    <Visibility />
                                  ) : (
                                    <VisibilityOff />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            }
                            labelWidth={145}
                          />
                        </FormControl>
                        <FormHelperText error>
                          <ErrorIcon color="error" fontSize="small" />
                          &nbsp; {errors.confirmPassword}
                        </FormHelperText>
                      </div>
                    ) : (
                      <FormControl
                        fullWidth
                        variant="outlined"
                        size="small"
                        margin="dense"
                      >
                        <InputLabel htmlFor="confirmPassword" required>
                          Confirm Password
                        </InputLabel>
                        <OutlinedInput
                          id="confirmPassword"
                          name="confirmPassword"
                          value={values.confirmPassword}
                          type={showConfirmPassword ? "text" : "password"}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowRPassword}
                                onMouseDown={handleMouseDownRPassword}
                                edge="end"
                              >
                                {showConfirmPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                          labelWidth={145}
                        />
                      </FormControl>
                    )}
                    <FormControlLabel
                      control={<Checkbox value="accepted" color="primary" />}
                      label="I have accepted the Term and Conditions"
                      margin="dense"
                    />
                    <br></br>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      className={classNamees.submit}
                      onClick={() => {
                        handleSubmit();
                      }}
                      disabled={!isValid || isSubmitting}
                    >
                      Create Account
                    </Button>
                  </FormGroup>
                  <Link href="/login" style={{ fontSize: 16 }}>
                    have an account? Sign In
                  </Link>
                  <Collapse
                    in={open}
                    style={{
                      width: 500,
                      marginTop: 20,
                      marginLeft: 50,
                    }}
                  >
                    <Alert
                      style={{
                        borderRadius: 20,
                      }}
                      action={
                        isSubmitting
                          ? setTimeout(() => {
                              open = true;
                              handleReset({});
                            }, 1500)
                          : setOpen(false)
                      }
                    >
                      Your account has been created successfully
                    </Alert>
                  </Collapse>
                  <Box mt={3}>
                    <Copyright />
                  </Box>
                </div>
              )}
            </Formik>
          </div>
          <div className="col-lg-6 d-none d-lg-block bg-login-image">
            <img
              alt="Identity App"
              src={signupImg}
              style={{
                marginTop: -76,
                height: "96.8%",
                marginLeft: 60,
                width: "111.5%",
                borderRadius: 10,
              }}
            />
          </div>
        </div>
      </CardBody>
    </Card>
  );
};

const mapStateToProps = ({ user }) => {
  return {
    saved: user.saved,
    error: user.error,
  };
};

const Signup = connect(
  mapStateToProps,
  { savedUser }
)(SignupPage);
export { Signup };
