import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as Yup from "yup";
import { updateProfile } from "../actions/register_actions";

//materiel Ui
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import { Form } from "reactstrap";
import { Paper, FormHelperText, Collapse } from "@material-ui/core";
import { Formik } from "formik";
import { Alert } from "@material-ui/lab";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <b>Identity.com </b>
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  centredPaper: {
    borderRadius: 40,
    padding: theme.spacing(4),
    border: "1px solid #dadce0",
    width: "60vw",
    height: "75vh",
    margin: 0,
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    overflowY: "auto",
  },
  title: {
    margin: theme.spacing(1, 0, 6),
    textAlign: "center",
    color: "SLATEGRAY",
    fontSize: 48,
  },
  submit: {
    height: 40,
    width: 220,
    fontSize: 15,
    border: "1px solid #dadce0",
    borderRadius: 20,
  },
  links: {
    fontSize: 18,
  },
}));

const EditProfilComponent = (props) => {
  // styles
  const classNamees = useStyles();
  const inputNames = {
    color: "DIMGRAY",
    fontSize: 18,
  };

  //states
  var [open, setOpen] = React.useState(false);

  let profile;
  try {
    profile = props.location.state.profile;
  } catch (e) {
    profile = undefined;
  }
  if (!profile) props.history.push("/home");

  const {
    managerName = "",
    workspaceName = "",
    category = "",
    description = "",
  } = profile;

  useEffect(() => {
    const { updated } = props;
    if (updated) {
      props.history.push("/home");
    }
  });

  return (
    <Paper className={classNamees.centredPaper}>
      <Typography className={classNamees.title} component="h1" variant="h2">
        <b>Edit Profil</b>
      </Typography>
      <Formik
        initialValues={{
          managerName,
          workspaceName,
          category,
          description,
        }}
        onSubmit={(values) => {
          const user = props.location.state.profile;
          values._id = user._id;
          props.updateProfile(values);
        }}
        validationSchema={Yup.object().shape({
          managerName: Yup.string().required(),
          workspaceName: Yup.string().required(),
          category: Yup.string().required(),
          description: Yup.string().required(),
        })}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          errors,
          touched,
          isValid,
          isSubmitting,
        }) => (
          <div>
            <Form style={{ paddingLeft: "15%" }}>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                  style={inputNames}
                >
                  Manager Name
                </label>
                <div className="col-sm-6">
                  <TextField
                    name="managerName"
                    variant="outlined"
                    required
                    fullWidth
                    label="Manager Name"
                    value={values.managerName}
                    autoComplete="managerName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    autoFocus
                  />
                  {errors.managerName && touched.managerName ? (
                    <FormHelperText error>{errors.managerName}</FormHelperText>
                  ) : null}
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                  style={inputNames}
                >
                  Workspace Name
                </label>
                <div className="col-sm-6">
                  <TextField
                    name="workspaceName"
                    variant="outlined"
                    required
                    fullWidth
                    label="Workspace Name"
                    value={values.workspaceName}
                    autoComplete="workspaceName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.workspaceName && touched.workspaceName ? (
                    <FormHelperText error>
                      {errors.workspaceName}
                    </FormHelperText>
                  ) : null}
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                  style={inputNames}
                >
                  Category
                </label>
                <div className="col-sm-6">
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="category"
                    label="Category"
                    name="category"
                    value={values.category}
                    autoComplete="category"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.category && touched.category ? (
                    <FormHelperText error>{errors.category}</FormHelperText>
                  ) : null}
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                  style={inputNames}
                >
                  Description
                </label>
                <div className="col-sm-6">
                  <TextField
                    variant="outlined"
                    fullWidth
                    required
                    id="description"
                    label="Description"
                    name="description"
                    value={values.description}
                    autoComplete="description"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    multiline
                    rows={3}
                    rowsMax={5}
                  />
                  {errors.description && touched.description ? (
                    <FormHelperText error>{errors.description}</FormHelperText>
                  ) : null}
                </div>
              </div>
            </Form>
            <br></br>
            <br />
            <div style={{ textAlign: "center" }}>
              <Button
                color="primary"
                variant="contained"
                className={classNamees.submit}
                onClick={handleSubmit}
                disabled={!isValid || isSubmitting}
              >
                {<SaveIcon />} APPLY EDIT
              </Button>
              &nbsp; &nbsp; &nbsp;
              <Link to="/home">
                <Button
                  color="secondary"
                  variant="contained"
                  className={classNamees.submit}
                >
                  {<CancelIcon />} Cancel
                </Button>
              </Link>
            </div>
            <Collapse
              in={open}
              style={{
                width: 500,
                marginLeft: "23%",
                marginTop: 20,
                borderRadius: 20,
              }}
            >
              <Alert
                style={{
                  borderRadius: 20,
                }}
                action={
                  isSubmitting
                    ? setTimeout(() => {
                        open = true;
                      }, 1500)
                    : setOpen(false)
                }
              >
                Profile updated successfully
              </Alert>
            </Collapse>
          </div>
        )}
      </Formik>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Paper>
  );
};

const mapStateToProps = ({ user }) => {
  return {
    updated: user.updated,
  };
};

const EditProfil = connect(
  mapStateToProps,
  { updateProfile }
)(EditProfilComponent);

export { EditProfil };
