import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchProduct } from "../actions/product_actions";

//material ui
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { Typography, Paper } from "@material-ui/core";
import CustomPromotCard from "components/CustomCard/CustomPromotCard";
import CustomCard from "components/CustomCard/CustomCard";
import CustomSidebar from "components/Sidebar/CustomSidebar";
import { Spinner, ProductItemList } from "../components";
import List from "@material-ui/core/List";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginLeft: theme.spacing(12.5),
      width: theme.spacing(189.5),
      height: "89.8vh",
      borderRadius: 40,
      border: "1px solid #dadce0",
    },
  },
  root1: {
    "& > *": {
      margin: theme.spacing(8.5, 6.6, 0),
      width: theme.spacing(80),
      height: theme.spacing(70),
      padding: theme.spacing(3, 3, 3),
      borderRadius: 20,
      border: "1px solid #dadce0",
    },
  },
  title: {
    textAlign: "left",
    color: "gray",
    margin: theme.spacing(10, 5, 0),
  },
  grid: {
    display: "flex",
    flexWrap: "wrap",
    backgroundColor: "#fff0",
    marginTop: theme.spacing(1),
    width: "99%",
    height: 500,
    overflowY: "auto",
  },
  list: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflowY: "auto",
    height: 450,
    paddingBottom: theme.spacing(1),
    width: "100%",
  },
}));

const defaultProps = {
  bgcolor: "SLATEBLUE",
  border: "1px solid #dadce0",
  borderRadius: 40,
  style: {
    width: "90vw",
    height: "90vh",
    spacing: 0,
    margin: 0,
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
};

const PromotProductsPage = (props) => {
  const classNamees = useStyles();

  useEffect(() => {
    const { fetchProduct } = props;
    fetchProduct();
  }, []);

  const { fetching, product } = props;

  const showSpinner = () => {
    if (fetching) {
      return <Spinner />;
    }
  };

  return (
    <Box {...defaultProps}>
      <div style={{ display: "flex", flexDirection: "rows" }}>
        <CustomSidebar />
        <div className={classNamees.root}>
          <Paper>
            <div style={{ display: "flex", flexDirection: "rows" }}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  height: "98vh",
                }}
              >
                <div className={classNamees.title}>
                  <Typography variant="h2" component="h3">
                    Choose Ads Campaing
                  </Typography>
                </div>
                <div className={classNamees.root1}>
                  <Paper>
                    <Typography
                      component="h5"
                      variant="h5"
                      align="left"
                      style={{
                        color: "gray",
                      }}
                    >
                      Promoted Products
                    </Typography>
                    <hr />
                    {showSpinner()}
                    <div className={classNamees.grid}>
                      <List className={classNamees.list}>
                        {product.map((item) =>
                          item.status !== false ? (
                            <ProductItemList key={item._id} item={item} />
                          ) : null
                        )}
                      </List>
                    </div>
                  </Paper>
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  height: "90vh",
                }}
              >
                <main className={classNamees.content}>
                  <div className={classNamees.appBarSpacer} />
                  <CustomPromotCard />
                </main>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  height: "90vh",
                  marginLeft: -3,
                }}
              >
                <main className={classNamees.content}>
                  <div className={classNamees.appBarSpacer} />
                  <CustomCard />
                </main>
              </div>
            </div>
          </Paper>
        </div>
      </div>
    </Box>
  );
};

const mapStateToProps = ({ product }) => {
  return {
    fetching: product.fetching,
    product: product.product,
  };
};

const PromotProducts = connect(
  mapStateToProps,
  { fetchProduct }
)(PromotProductsPage);
export { PromotProducts };
