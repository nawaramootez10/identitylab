import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchCustomer } from "../actions/community_actions";
import { Spinner, CustomerItem } from "../components";

//Material UI
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { Typography, Paper } from "@material-ui/core";
import { ListGroup } from "reactstrap";
import CustomSidebar from "components/Sidebar/CustomSidebar";
import CustomCard from "components/CustomCard/CustomCard";
import CustomCustomersCard from "components/CustomCard/CustomCustomersCard";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginLeft: theme.spacing(12.5),
      width: theme.spacing(189.5),
      height: "89.8vh",
      borderRadius: 40,
      border: "1px solid #dadce0",
    },
  },
  root1: {
    "& > *": {
      margin: theme.spacing(8.5, 6.6, 0),
      width: theme.spacing(80),
      height: theme.spacing(70),
      padding: theme.spacing(3, 3, 3),
      borderRadius: 20,
      border: "1px solid #dadce0",
    },
  },
  title: {
    textAlign: "right",
    color: "gray",
    margin: theme.spacing(10, 5, 0),
  },
}));

const defaultProps = {
  bgcolor: "SLATEBLUE",
  border: "1px solid #dadce0",
  borderRadius: 40,
  style: {
    width: "90vw",
    height: "90vh",
    spacing: 0,
    margin: 0,
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
};

const CommunityComponent = (props) => {
  const classNamees = useStyles();

  useEffect(() => {
    const { fetchCustomer } = props;
    fetchCustomer();
  }, []);

  const { fetching, community } = props;

  const showSpinner = () => {
    if (fetching) {
      return <Spinner />;
    }
  };

  return (
    <Box {...defaultProps}>
      <div style={{ display: "flex", flexDirection: "rows" }}>
        <CustomSidebar />
        <div className={classNamees.root}>
          <Paper>
            <div style={{ display: "flex", flexDirection: "rows" }}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  height: "90vh",
                }}
              >
                <div className={classNamees.title}>
                  <Typography component="h3" variant="h2">
                    COMMUNITY
                  </Typography>
                </div>
                <div className={classNamees.root1}>
                  <Paper>
                    <Typography
                      component="h5"
                      variant="h5"
                      align="left"
                      style={{ color: "gray" }}
                    >
                      Latest Added Customers:
                    </Typography>
                    <hr />
                    {showSpinner()}
                    <ListGroup style={{ overflowY: "auto" }}>
                      {community.map((item) => (
                        <CustomerItem key={item._id} item={item} />
                      ))}
                    </ListGroup>
                  </Paper>
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  height: "90vh",
                }}
              >
                <main className={classNamees.content}>
                  <div className={classNamees.appBarSpacer} />
                  <CustomCustomersCard />
                </main>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  height: "90vh",
                  marginLeft: -3,
                }}
              >
                <main className={classNamees.content}>
                  <div className={classNamees.appBarSpacer} />
                  <CustomCard />
                </main>
              </div>
            </div>
          </Paper>
        </div>
      </div>
    </Box>
  );
};

const mapStateToProps = ({ community }) => {
  return {
    fetching: community.fetching,
    community: community.community,
  };
};

const Community = connect(
  mapStateToProps,
  { fetchCustomer }
)(CommunityComponent);
export { Community };
