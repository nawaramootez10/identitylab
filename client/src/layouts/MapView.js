import React from "react";
import SimpleMap from "components/Map/SimpleMap.js";
//material ui
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { Paper } from "@material-ui/core";
import CustomSidebar from "components/Sidebar/CustomSidebar";
import CustomCard from "components/CustomCard/CustomCard";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginLeft: theme.spacing(12.5),
      width: theme.spacing(189.6),
      height: "89.8vh",
      borderRadius: 40,
      border: "1px solid #dadce0",
    },
  },
}));

const defaultProps = {
  bgcolor: "SLATEBLUE",
  border: "1px solid #dadce0",
  borderRadius: 40,
  style: {
    width: "90vw",
    height: "90vh",
    spacing: 0,
    margin: 0,
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
};

function MapView() {
  const classNamees = useStyles();
  return (
    <Box {...defaultProps}>
      <div style={{ display: "flex", flexDirection: "rows" }}>
        <CustomSidebar />
        <div className={classNamees.root}>
          <Paper>
            <div style={{ display: "flex", flexDirection: "rows" }}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  width: "100vw",
                }}
              >
                <SimpleMap />
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  height: "90vh",
                  width: 470,
                  marginRight: -28,
                  marginLeft: 8,
                }}
              >
                <main className={classNamees.content}>
                  <div className={classNamees.appBarSpacer} />
                  <CustomCard />
                </main>
              </div>
            </div>
          </Paper>
        </div>
      </div>
    </Box>
  );
}

export { MapView };
