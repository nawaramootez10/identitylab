import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  fetchProduct,
  deleteProduct,
  promoteProduct,
} from "../actions/product_actions";
import { fetchPromotion, savedPromotion } from "../actions/promotion_actions";

//materiel Ui
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import CustomSidebar from "components/Sidebar/CustomSidebar";
import { Spinner, ProductItem } from "../components";
import { Divider } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import Collapse from "@material-ui/core/Collapse";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      height: "89.8vh",
      width: "79vw",
      marginLeft: theme.spacing(12.5),
      padding: theme.spacing(3, 5, 0),
      borderRadius: 40,
      border: "1px solid #dadce0",
    },
  },
  grid: {
    display: "flex",
    flexWrap: "wrap",
    overflow: "hidden",
    flexDirection: "column",
    justifyContent: "space-around",
    backgroundColor: "#fff0",
  },
  gridlist: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflowY: "auto",
    height: 675,
    paddingBottom: theme.spacing(1),
  },
  title: {
    textAlign: "center",
    color: "gray",
  },
  submit: {
    margin: theme.spacing(-4, 0, 0),
    height: 45,
    width: 280,
    fontSize: 20,
    color: "white",
    backgroundColor: "SLATEBLUE",
    borderRadius: 30,
    border: "1px solid #dadce0",
    position: "fixed",
    bottom: 15,
    left: "49%",
    cursor: "pointer",
  },
}));

const defaultProps = {
  bgcolor: "SLATEBLUE",
  border: "1px solid #dadce0",
  borderRadius: 40,
  style: {
    height: "90vh",
    width: "90vw",
    spacing: 0,
    margin: 0,
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
};

const MyProductsComponent = (props) => {
  // styles
  const classNamees = useStyles();

  //States
  const [open, setOpen] = useState(false);

  useEffect(() => {
    const { fetchProduct } = props;
    fetchProduct();
  }, []);

  const { fetching, product } = props;

  const showSpinner = () => {
    if (fetching) {
      return <Spinner />;
    }
  };

  return (
    <Box {...defaultProps}>
      <div style={{ display: "flex", flexDirection: "rows" }}>
        <CustomSidebar />
        <div style={{ display: "flex", flexDirection: "column" }}>
          <div className={classNamees.root}>
            <Paper>
              <div className={classNamees.title}>
                <Typography component="h1" variant="h3">
                  PRODUCT LIST
                </Typography>
              </div>
              <Divider
                style={{
                  height: 2,
                  marginBottom: 15,
                  marginTop: 10,
                }}
              />
              {showSpinner()}
              <div className={classNamees.grid}>
                <GridList
                  cellHeight="auto"
                  cols={4}
                  className={classNamees.gridlist}
                >
                  {product.map((item) => (
                    <ProductItem
                      key={item._id}
                      item={item}
                      onDelete={() => {
                        const { deleteProduct } = props;
                        deleteProduct(item._id);
                        setTimeout(() => {
                          setOpen(true);
                        }, 1000);
                      }}
                      onPromote={() => {
                        const { savedPromotion, promoteProduct } = props;

                        const now = new Date();

                        const startTime = new Date(
                          now.getFullYear(),
                          now.getMonth(),
                          now.getDate(),
                          now.getHours()
                        );
                        const endTime = new Date(
                          now.getFullYear(),
                          now.getMonth(),
                          now.getDate() + 1,
                          now.getHours()
                        );

                        const values = {
                          startTime: startTime,
                          endTime: endTime,
                          promotionState: true,
                          prod: item._id,
                        };

                        savedPromotion(values);
                        const data = { status: true };
                        data._id = item._id;
                        promoteProduct(data);
                        console.log("Product promoted");
                        setTimeout(() => {
                          window.location.reload();
                        }, 2000);
                      }}
                    />
                  ))}
                </GridList>
                <Collapse
                  in={open}
                  style={{
                    alignItems: "center",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Alert
                    style={{
                      width: 400,
                      borderRadius: 20,
                      zIndex: 1,
                      position: "absolute",
                      left: "45%",
                    }}
                    action=""
                    onClose={setTimeout(() => {
                      setOpen(false);
                    }, 2000)}
                  >
                    Product Deleted successfully
                  </Alert>
                </Collapse>
              </div>
              <Link to="/myProducts/addProduct">
                <Button
                  type="submit"
                  variant="contained"
                  className={classNamees.submit}
                >
                  Add More Products
                </Button>
              </Link>
            </Paper>
          </div>
        </div>
      </div>
    </Box>
  );
};

const mapStateToProps = ({ product }) => {
  return {
    fetching: product.fetching,
    product: product.product,
  };
};

const MyProducts = connect(
  mapStateToProps,
  {
    fetchProduct,
    deleteProduct,
    promoteProduct,
    fetchPromotion,
    savedPromotion,
  }
)(MyProductsComponent);
export { MyProducts };
