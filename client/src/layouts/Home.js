import React, { useEffect } from "react";
import { connect } from "react-redux";
import { logUserOut, getUserProfile } from "../actions/auth_actions";
import { fetchLatestProduct } from "../actions/product_actions";

import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { Typography, Paper } from "@material-ui/core";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { Avatar } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import CustomCard from "components/CustomCard/CustomCard";
import { QRCode } from "react-qr-svg";
import { Spinner, ProductItemList } from "../components";
import List from "@material-ui/core/List";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginLeft: theme.spacing(1),
      width: theme.spacing(161.4),
      height: "89.8vh",
      borderRadius: 40,
      border: "1px solid #dadce0",
    },
  },
  root1: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(5, 6.9),
      width: theme.spacing(89.9),
      height: theme.spacing(70),
      borderRadius: 20,
      border: "1px solid #dadce0",
    },
  },
  btn: {
    width: 300,
    height: 50,
    borderRadius: 20,
    color: "SLATEBLUE",
    backgroundColor: "white",
    border: "1px solid #dadce0",
  },
  leftBtn: {
    width: 180,
    borderRadius: 100,
    border: "1px solid #dadce0",
  },
  btnContainer: {
    display: "flex",
    flexDirection: "row",
    position: "relative",
    marginTop: theme.spacing(2),
    justifyContent: "center",
  },
  listItem: {
    color: "SPRINGGREEN",
    fontSize: "18px",
  },
  bussinessInfo: {
    color: "white",
    fontSize: "19px",
  },
  grid: {
    display: "flex",
    flexWrap: "wrap",
    overflow: "hidden",
    backgroundColor: "#fff0",
    marginTop: theme.spacing(1),
    padding: "0px 10px",
    width: "99%",
  },
  list: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflowY: "auto",
    height: 450,
    paddingBottom: theme.spacing(1),
    width: "100%",
  },
}));

const defaultProps = {
  bgcolor: "SLATEBLUE",
  border: "1px solid #dadce0",
  borderRadius: 40,
  style: {
    width: "90vw",
    height: "90vh",
    spacing: 0,
    margin: 0,
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
};

const HomePage = (props) => {
  const classNamees = useStyles();

  const { profile } = props;

  const renderLoginOrLogout = () => {
    const { isAuth } = props;
    if (isAuth) {
      return (
        <Typography
          variant="button"
          style={{
            marginLeft: "30px",
            marginTop: "49px",
            color: "white",
            fontSize: "30px",
          }}
        >
          {profile.workspaceName}
        </Typography>
      );
    }
  };

  useEffect(() => {
    const { fetchLatestProduct, getUserProfile } = props;
    fetchLatestProduct();
    getUserProfile();
  }, []);

  const { fetching, product } = props;

  const showSpinner = () => {
    if (fetching) {
      return <Spinner />;
    }
  };

  return (
    <Box {...defaultProps}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            height: "90vh",
          }}
        >
          <div
            style={{ display: "flex", flexWrap: "wrap", flexDirection: "row" }}
          >
            <Avatar
              style={{
                marginLeft: "50px",
                marginTop: "45px",
                width: "60px",
                height: "60px",
                backgroundColor: "white",
                color: "black",
                fontSize: 30,
              }}
            >
              {("" + profile.workspaceName + "").substr(0, 1)}
            </Avatar>
            {renderLoginOrLogout()}
          </div>
          <br></br>
          <br></br>
          <Divider
            style={{
              backgroundColor: "white",
              width: 420,
              height: 2,
              marginLeft: 8,
            }}
          />
          <div
            style={{
              display: "flex",
              flexWrap: "wrap",
              flexDirection: "column",
              height: "65vh",
            }}
          >
            <br></br>
            <div style={{ display: "flex", flexDirection: "column" }}>
              <div style={{ display: "flex", flexDirection: "row" }}>
                <div className="col-5">
                  <Typography className={classNamees.listItem}>
                    Manager Name :
                  </Typography>
                </div>
                <Typography className={classNamees.bussinessInfo}>
                  {profile.managerName}
                </Typography>
              </div>
              <br></br>
              <div style={{ display: "flex", flexDirection: "row" }}>
                <div className="col-sm-5">
                  <Typography className={classNamees.listItem}>
                    Email:
                  </Typography>
                </div>
                <Typography className={classNamees.bussinessInfo}>
                  {profile.email}
                </Typography>
              </div>
            </div>
            <Divider
              style={{
                backgroundColor: "white",
                width: 420,
                height: 2,
                marginLeft: 8,
              }}
            />
            <br></br>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div className="col-sm-5">
                <Typography className={classNamees.listItem}>
                  Category:
                </Typography>
              </div>
              <Typography className={classNamees.bussinessInfo}>
                {profile.category}
              </Typography>
            </div>
            <br></br>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div className="col-sm-5">
                <Typography className={classNamees.listItem}>
                  Description:
                </Typography>
              </div>
              <Typography className={classNamees.bussinessInfo}>
                {profile.description}
              </Typography>
            </div>
            <br></br>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div className="col-sm-5">
                <Typography
                  className={classNamees.listItem}
                  style={{ marginTop: 30 }}
                >
                  QR_Code:
                </Typography>
              </div>
              <QRCode
                bgColor="SLATEBLUE"
                fgColor="#000000"
                level="Q"
                style={{ width: 125 }}
                value={profile.email + ""}
              />
            </div>
          </div>
          <div className={classNamees.btnContainer}>
            <Link
              to={{
                pathname: "/editProfil",
                state: { profile },
                search:
                  "?id=" +
                  profile._id +
                  "?managerName=" +
                  profile.managerName +
                  "?workspaceName=" +
                  profile.workspaceName,
              }}
            >
              <Button
                variant="contained"
                className={classNamees.leftBtn}
                style={{ backgroundColor: "white", color: "SLATEBLUE" }}
              >
                Edit Profil
              </Button>
            </Link>
            <Button
              color="secondary"
              variant="contained"
              className={classNamees.leftBtn}
              style={{ marginLeft: 25 }}
              onClick={() => logUserOut()}
            >
              Sign Out
            </Button>
          </div>
        </div>
        <div className={classNamees.root}>
          <Paper>
            <div style={{ display: "flex", flexDirection: "rows" }}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  height: "90vh",
                  marginLeft: 20,
                }}
              >
                <div className={classNamees.root1}>
                  <Paper>
                    <Typography
                      component="h5"
                      variant="h5"
                      align="left"
                      style={{
                        color: "DIMGRAY",
                        marginLeft: 30,
                        marginTop: 20,
                      }}
                    >
                      Latest Added Product / Services
                    </Typography>
                    <hr />
                    {showSpinner()}
                    <div className={classNamees.grid}>
                      <List className={classNamees.list}>
                        {product.map((item) => (
                          <ProductItemList key={item._id} item={item} />
                        ))}
                      </List>
                    </div>
                  </Paper>
                </div>
                <div
                  style={{
                    display: "flex",
                    flexWrap: "wrap",
                    flexDirection: "column",
                    marginLeft: 250,
                  }}
                >
                  <Link to="/myProducts">
                    <Button variant="contained" className={classNamees.btn}>
                      My Products
                    </Button>
                  </Link>
                  <br></br>
                  <Link to="/community">
                    <Button variant="contained" className={classNamees.btn}>
                      Community
                    </Button>
                  </Link>
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  height: "90vh",
                  marginLeft: -3,
                }}
              >
                <main className={classNamees.content}>
                  <div className={classNamees.appBarSpacer} />
                  <CustomCard />
                </main>
              </div>
            </div>
          </Paper>
        </div>
      </div>
    </Box>
  );
};

const mapStateToProps = ({ auth, product }) => {
  return {
    isAuth: auth.isAuth,
    profile: auth.profile,
    fetching: product.fetching,
    product: product.product,
  };
};

const Home = connect(
  mapStateToProps,
  { logUserOut, fetchLatestProduct, getUserProfile }
)(HomePage);
export { Home };
