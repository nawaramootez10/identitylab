import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";

// core components
import "leaflet/dist/leaflet.css";
import App from "App";
import * as serviceWorker from "./serviceWorker";
import store from "./store";
import { onLodingSignIn } from "./actions";

store.dispatch(onLodingSignIn());

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);

serviceWorker.unregister();
