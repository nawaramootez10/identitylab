import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import {
  Login,
  Home,
  AddCustomer,
  MyProducts,
  AddProduct,
  EditProduct,
  Community,
  EditProfil,
  PromotProducts,
  ShowCustomer,
  MapView,
  Signup,
} from "./layouts";
import { ProtectedRoute } from "./components";

class App extends Component {
  render() {
    return (
      <div style={{ minHeight: "100vh" }}>
        <Switch>
          <Redirect from="/" to="/home" exact />
          <ProtectedRoute path="/home" component={Home} exact />
          <ProtectedRoute path="/myProducts" component={MyProducts} exact />
          <ProtectedRoute
            path="/myProducts/addProduct"
            component={AddProduct}
            exact
          />
          <ProtectedRoute
            path="/myProducts/editProduct"
            component={EditProduct}
            exact
          />
          <ProtectedRoute
            path="/community/addCustomer"
            component={AddCustomer}
            exact
          />
          <ProtectedRoute path="/community" component={Community} exact />
          <ProtectedRoute path="/editProfil" component={EditProfil} exact />
          <ProtectedRoute
            path="/promotProducts"
            component={PromotProducts}
            exact
          />
          <ProtectedRoute
            path="/community/showCustomer"
            component={ShowCustomer}
            exact
          />
          <ProtectedRoute path="/mapView" component={MapView} exact />
        </Switch>
        <Route path="/signup" component={Signup} exact />
        <Route path="/login" component={Login} exact />
      </div>
    );
  }
}

export default App;
