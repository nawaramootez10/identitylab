import {
  PRODUCT_SAVED,
  RESET_SAVED_FLAG,
  FETCHED_FAILED,
  FETCHED_SUCCESS,
  FETCHING_PRODUCT,
  PRODUCT_UPDATED,
  PRODUCT_PROMOTED,
} from "../actions/types";

const INITIAL_STATE = {
  updated: false,
  promoted: false,
  saved: false,
  fetching: false,
  product: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCHING_PRODUCT:
      return { ...state, fetching: true };
    case FETCHED_SUCCESS:
      return { ...state, fetching: false, product: action.payload };
    case FETCHED_FAILED:
      return { ...state, fetching: false };
    case PRODUCT_SAVED:
      return { ...state, saved: true };
    case PRODUCT_UPDATED:
      return { ...state, updated: true };
    case PRODUCT_PROMOTED:
      return { ...state, promoted: true };
    case RESET_SAVED_FLAG:
      return { ...state, saved: false, updated: false };
    default:
      return state;
  }
};
