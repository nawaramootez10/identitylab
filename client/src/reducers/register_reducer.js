import { USER_SAVED, REGISTER_FAILED, USER_UPDATED } from "../actions/types";

const INITIAL_STATE = {
  saved: false,
  error: null,
  updated: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_SAVED:
      return { ...state, saved: true, error: null };
    case REGISTER_FAILED:
      return { ...state, saved: false, error: action.payload };
    case USER_UPDATED:
      return { ...state, updated: true };
    default:
      return state;
  }
};
