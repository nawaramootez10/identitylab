import { combineReducers } from "redux";

import auth from "./auth_reducer";
import user from "./register_reducer";
import product from "./product_reducer";
import community from "./community_reducer";
import errors from "./error_reducer";

export default combineReducers({
  auth,
  user,
  product,
  community,
  errors,
});
