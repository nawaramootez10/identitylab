import { FETCHING_PROMOTION, PROMOTION_SAVED } from "../actions/types";

const INITIAL_STATE = {
  saved: false,
  fetching: false,
  promotion: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCHING_PROMOTION:
      return { ...state, fetching: true };
    case FETCHED_SUCCESS:
      return { ...state, fetching: false, promotion: action.payload };
    case FETCHED_FAILED:
      return { ...state, fetching: false };
    case PROMOTION_SAVED:
      return { ...state, saved: true };
    default:
      return state;
  }
};
