import {
  COMMUNITY_SAVED,
  FETCHING_COMMUNITY,
  COMMUNITY_FETCHED_FAILED,
  COMMUNITY_FETCHED_SUCCESS,
  RESET_SAVED_FLAG,
} from "../actions/types";

const INITIAL_STATE = {
  saved: false,
  fetching: false,
  community: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCHING_COMMUNITY:
      return { ...state, fetching: true };
    case COMMUNITY_FETCHED_SUCCESS:
      return { ...state, fetching: false, community: action.payload };
    case COMMUNITY_FETCHED_FAILED:
      return { ...state, fetching: false };
    case COMMUNITY_SAVED:
      return { ...state, saved: true };
    case RESET_SAVED_FLAG:
      return { ...state, saved: false };
    default:
      return state;
  }
};
