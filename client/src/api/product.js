import axios from "axios";

export const apiSaveProduct = (product) => {
  return axios.post("/api/v1/product", product);
};

export const apiUpdateProduct = (product) => {
  return axios.put("/api/v1/product/" + product._id, product);
};

export const apiDeleteProduct = (productId) => {
  return axios.delete("/api/v1/product/" + productId);
};

export const apiPromoteProduct = (product) => {
  return axios.put("/api/v1/product/promote/" + product._id, product);
};

export const apiFetchProduct = () => {
  return axios.get("/api/v1/product");
};

export const apiFetchLatestProduct = () => {
  return axios.get("/api/v1/latestproduct");
};
