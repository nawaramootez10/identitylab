import axios from "axios";

export const apiLogin = (request_data) => {
  return axios.post("/api/v1/login", request_data);
};

export const fetchProfile = (token) => {
  return axios.get("/api/v1/me", token);
};

export const apiSaveUser = (user) => {
  return axios.post("/api/v1/register", user);
};

export const apiUpdateProfile = (user) => {
  return axios.put("/api/v1/register/" + user._id, user);
};
