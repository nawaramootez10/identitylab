import axios from "axios";

export const apiSavePromotion = (promotion) => {
  return axios.post("/api/v1/promotion", promotion);
};

export const apiFetchPromotion = () => {
  return axios.get("/api/v1/promotion");
};
