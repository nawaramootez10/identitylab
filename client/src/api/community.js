import axios from "axios";

export const apiSaveCustomer = (community) => {
  return axios.post("/api/v1/community", community);
};

export const apiDeleteCustomer = (communityId) => {
  return axios.delete("/api/v1/community/" + communityId);
};

export const apiFetchCommunity = () => {
  return axios.get("/api/v1/community");
};
