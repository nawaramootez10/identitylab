import {
  PROMOTION_SAVED,
  FETCHING_PROMOTION,
  FETCHED_SUCCESS,
  FETCHED_FAILED,
} from "./types";
import { apiSavePromotion, apiFetchPromotion } from "api/promotion";

export const savedPromotion = (promotion) => {
  return async (dispatch) => {
    try {
      await apiSavePromotion(promotion);
      dispatch({ type: PROMOTION_SAVED });
    } catch (e) {
      console.error(e);
    }
  };
};

export const fetchPromotion = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: FETCHING_PROMOTION });
      const { data } = await apiFetchPromotion();
      dispatch({ type: FETCHED_SUCCESS, payload: data.promotion });
    } catch (e) {
      dispatch({ type: FETCHED_FAILED });
    }
  };
};
