import { USER_SAVED, REGISTER_FAILED, USER_UPDATED } from "./types";
import { apiSaveUser, apiUpdateProfile } from "api/user";
import { addErrorMessage } from "./error_actions";

export const savedUser = (user) => {
  return async (dispatch) => {
    try {
      await apiSaveUser(user);
      dispatch({ type: USER_SAVED });
    } catch (e) {
      const {
        response: { data },
      } = e;
      dispatch(error(data.error));
    }
  };
};

export const updateProfile = (user) => {
  return async (dispatch) => {
    try {
      await apiUpdateProfile(user);
      dispatch({ type: USER_UPDATED });
    } catch (e) {
      dispatch(addErrorMessage(e));
    }
  };
};

const error = (error) => {
  return { type: REGISTER_FAILED, payload: error };
};
