import {
  PRODUCT_SAVED,
  RESET_SAVED_FLAG,
  FETCHING_PRODUCT,
  FETCHED_SUCCESS,
  FETCHED_FAILED,
  PRODUCT_UPDATED,
  PRODUCT_PROMOTED,
} from "./types";
import { addErrorMessage, clearErrorMessages } from "./error_actions";
import {
  apiSaveProduct,
  apiFetchProduct,
  apiFetchLatestProduct,
  apiUpdateProduct,
  apiDeleteProduct,
  apiPromoteProduct,
} from "api/product";

export const savedProduct = (product) => {
  return async (dispatch) => {
    try {
      dispatch(clearErrorMessages());
      await apiSaveProduct(product);
      dispatch({ type: PRODUCT_SAVED });
    } catch (e) {
      dispatch(addErrorMessage(e));
    }
  };
};

export const updateProduct = (product) => {
  return async (dispatch) => {
    try {
      dispatch(clearErrorMessages());
      await apiUpdateProduct(product);
      dispatch({ type: PRODUCT_UPDATED });
    } catch (e) {
      dispatch(addErrorMessage(e));
    }
  };
};

export const promoteProduct = (product) => {
  return async (dispatch) => {
    try {
      dispatch(clearErrorMessages());
      await apiPromoteProduct(product);
      dispatch({ type: PRODUCT_PROMOTED });
    } catch (e) {
      dispatch(addErrorMessage(e));
    }
  };
};

export const fetchProduct = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: FETCHING_PRODUCT });
      const { data } = await apiFetchProduct();
      dispatch({ type: FETCHED_SUCCESS, payload: data.product });
    } catch (e) {
      dispatch({ type: FETCHED_FAILED });
      dispatch(addErrorMessage(e));
    }
  };
};

export const fetchLatestProduct = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: FETCHING_PRODUCT });
      const { data } = await apiFetchLatestProduct();
      dispatch({ type: FETCHED_SUCCESS, payload: data.product });
    } catch (e) {
      dispatch({ type: FETCHED_FAILED });
      dispatch(addErrorMessage(e));
    }
  };
};

export const deleteProduct = (productId) => {
  return async (dispatch) => {
    try {
      dispatch(clearErrorMessages());
      await apiDeleteProduct(productId);
      dispatch(fetchProduct());
    } catch (e) {
      dispatch(addErrorMessage(e));
    }
  };
};

export const resetSaved = () => ({ type: RESET_SAVED_FLAG });
