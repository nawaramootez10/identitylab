import {
  COMMUNITY_SAVED,
  RESET_SAVED_FLAG,
  FETCHING_COMMUNITY,
  COMMUNITY_FETCHED_SUCCESS,
  COMMUNITY_FETCHED_FAILED,
} from "./types";
import { addErrorMessage, clearErrorMessages } from "./error_actions";
import {
  apiSaveCustomer,
  apiFetchCommunity,
  apiDeleteCustomer,
} from "api/community";

export const savedCustomer = (community) => {
  return async (dispatch) => {
    try {
      dispatch(clearErrorMessages());
      await apiSaveCustomer(community);
      dispatch({ type: COMMUNITY_SAVED });
    } catch (e) {
      dispatch(addErrorMessage(e));
    }
  };
};

export const fetchCustomer = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: FETCHING_COMMUNITY });
      const { data } = await apiFetchCommunity();
      dispatch({ type: COMMUNITY_FETCHED_SUCCESS, payload: data.community });
    } catch (e) {
      dispatch({ type: COMMUNITY_FETCHED_FAILED });
      dispatch(addErrorMessage(e));
    }
  };
};

export const deleteCustomer = (communityId) => {
  return async (dispatch) => {
    try {
      dispatch(clearErrorMessages());
      await apiDeleteCustomer(communityId);
      dispatch(fetchCustomer());
    } catch (e) {
      dispatch(addErrorMessage(e));
    }
  };
};

export const resetSaved = () => ({ type: RESET_SAVED_FLAG });
