import React from "react";

//Material UI
import { ListGroupItem } from "reactstrap";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    height: 50,
    marginBottom: theme.spacing(1),
    borderRadius: 15,
    border: "1px solid #dadce0",
    cursor: "pointer",
  },
}));

const CustomerItem = ({ item }) => {
  const classes = useStyles();

  return (
    <ListGroupItem className={classes.root}>
      <div className="float-left" style={{ fontSize: 16 }}>
        <span style={{ color: "SLATEBLUE" }}>Customer Name:</span>{" "}
        <span style={{ letterSpacing: 0.5, fontWeight: "bold" }}>
          {item.customerName}
        </span>
      </div>
      <div className="float-right" style={{ fontSize: 16 }}>
        <span style={{ color: "SLATEBLUE" }}>E-mail:</span>{" "}
        <span style={{ color: "BLACK", letterSpacing: 0.5 }}>
          {item.customerEmail ? item.customerEmail : "not available email"}
        </span>
      </div>
    </ListGroupItem>
  );
};

export { CustomerItem };
