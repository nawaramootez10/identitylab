import React from "react";
import { Link } from "react-router-dom";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Avatar } from "@material-ui/core";
import AppsIcon from "@material-ui/icons/Apps";
import LocalMallIcon from "@material-ui/icons/LocalMall";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import FolderIcon from "@material-ui/icons/Folder";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";

const useStyles = makeStyles((theme) => ({
  avatar: {
    marginLeft: "50px",
    marginTop: "60px",
    width: "60px",
    height: "60px",
    backgroundColor: "white",
    color: "black",
    fontSize: 30,
  },
  icons: {
    color: "LAVENDER",
    width: 27,
    height: 27,
  },
}));

export default function CustomSidebar(props) {
  const classNamees = useStyles();
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Link to="/home">
        <Avatar className={classNamees.avatar}>
          <b>I</b>
        </Avatar>
      </Link>
      <br></br>
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          flexDirection: "column",
          marginLeft: 45,
        }}
      >
        <br></br>
        <br></br>
        <br></br>
        <Link to="/home" exact="true">
          <Button>
            <AppsIcon className={classNamees.icons} />
          </Button>
        </Link>
        <br></br>
        <br></br>
        <br></br>
        <Link to="/myProducts" exact="true">
          <Button>
            <LocalMallIcon className={classNamees.icons} />
          </Button>
        </Link>
        <br></br>
        <br></br>
        <br></br>
        <Link to="/promotProducts" exact="true">
          <Button>
            <ShoppingCartIcon className={classNamees.icons} />
          </Button>
        </Link>
        <br></br>
        <br></br>
        <br></br>
        <Link to="/community" exact="true">
          <Button>
            <FolderIcon className={classNamees.icons} />
          </Button>
        </Link>
        <br></br>
        <br></br>
        <br></br>
        <Link to="/mapView" exact="true">
          <Button>
            <LocalOfferIcon className={classNamees.icons} />
          </Button>
        </Link>
      </div>
    </div>
  );
}
