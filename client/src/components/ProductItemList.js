import React from "react";

//Material UI
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import { Badge, Row, Col } from "reactstrap";
import { Avatar } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    width: 500,
    marginBottom: theme.spacing(1),
    border: "1px solid #dadce0",
    borderRadius: 10,
    cursor: "pointer",
  },
  media: {
    height: 80,
    width: 120,
  },
}));

const ProductItemList = ({ item }) => {
  const classNamees = useStyles();

  return (
    <div>
      <ListItem className={classNamees.root}>
        <Row>
          <Col>
            <ListItemAvatar>
              <Avatar
                src={item.productImage}
                alt={item.productName}
                variant="square"
                className={classNamees.media}
              />
            </ListItemAvatar>
          </Col>
          <div style={{ padding: "8px 15px" }}>
            <ListItemText
              primary={item.productName}
              secondary={
                <Badge color="dark" style={{ fontSize: 15 }}>
                  {item.productOnSalePrice}
                  {" TND"}
                </Badge>
              }
            />
          </div>
        </Row>
      </ListItem>
    </div>
  );
};

export { ProductItemList };
