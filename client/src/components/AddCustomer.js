import React, { useState, useEffect } from "react";
import * as Yup from "yup";
import {
  savedCustomer,
  resetSaved,
  fetchCustomer,
} from "../actions/community_actions";
import { connect } from "react-redux";
import { ErrorMessage } from "../components";

import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import {
  makeStyles,
  Typography,
  FormGroup,
  TextField,
} from "@material-ui/core";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import { Formik } from "formik";

const useStyles = makeStyles((theme) => ({
  bigIcons: {
    margin: theme.spacing(6, 9, 4),
    fontSize: 100,
  },
  modalcolor: {
    color: "SLATEBLUE",
  },
}));

const AddCustomerComponent = (props) => {
  const classNamees = useStyles();

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  useEffect(() => {
    const { saved, fetchCustomer } = props;

    if (saved && modal) {
      toggle();
      fetchCustomer();
      resetSaved();
    }
  });

  return (
    <div>
      <div
        onClick={toggle}
        style={{ display: "flex", flexDirection: "column", cursor: "pointer" }}
      >
        <GroupAddIcon className={classNamees.bigIcons} color="action" />
        <Typography
          component="h5"
          variant="h6"
          align="center"
          style={{ color: "gray" }}
        >
          Add New Customer
        </Typography>
      </div>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle} className={classNamees.modalcolor}>
          Add New Customer
        </ModalHeader>
        <ModalBody>
          <ErrorMessage />
          <Formik
            initialValues={{
              customerName: "",
              customerEmail: "",
            }}
            onSubmit={(values) => {
              props.savedCustomer(values);
            }}
            validationSchema={Yup.object().shape({
              customerName: Yup.string().required(),
              customerEmail: Yup.string()
                .email()
                .required(),
            })}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              errors,
              touched,
              isValid,
              isSubmitting,
            }) => (
              <div>
                <FormGroup>
                  {errors.customerName && touched.customerName ? (
                    <TextField
                      className={classNamees.textfild}
                      variant="outlined"
                      required
                      error
                      helperText={errors.customerName}
                      label="Customer Name"
                      name="customerName"
                      value={values.customerName}
                      autoComplete="customerName"
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  ) : (
                    <TextField
                      className={classNamees.textfild}
                      variant="outlined"
                      required
                      label="Customer Name"
                      name="customerName"
                      value={values.customerName}
                      autoComplete="customerName"
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  )}
                  <br></br>
                  {errors.customerEmail && touched.customerEmail ? (
                    <TextField
                      className={classNamees.textfild}
                      variant="outlined"
                      required
                      error
                      helperText={errors.customerEmail}
                      label="Customer Email"
                      name="customerEmail"
                      value={values.customerEmail}
                      autoComplete="customerEmail"
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  ) : (
                    <TextField
                      className={classNamees.textfild}
                      variant="outlined"
                      required
                      label="Customer Email"
                      name="customerEmail"
                      value={values.customerEmail}
                      autoComplete="customerEmail"
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  )}
                </FormGroup>
                <hr></hr>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <Button
                    color="primary"
                    onClick={handleSubmit}
                    disabled={!isValid || isSubmitting}
                  >
                    Add Customer
                  </Button>
                  &nbsp;&nbsp;
                  <Button color="secondary" onClick={toggle}>
                    Cancel
                  </Button>
                </div>
              </div>
            )}
          </Formik>
        </ModalBody>
      </Modal>
    </div>
  );
};

const mapStateToProps = ({ community, errors }) => {
  return {
    saved: community.saved,
    error: errors.message,
  };
};

const AddCustomer = connect(
  mapStateToProps,
  { savedCustomer, fetchCustomer }
)(AddCustomerComponent);
export { AddCustomer };
