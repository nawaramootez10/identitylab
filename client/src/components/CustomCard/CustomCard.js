import React from "react";
import { Link } from "react-router-dom";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { Typography, Paper } from "@material-ui/core";
import PeopleIcon from "@material-ui/icons/People";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import Button from "@material-ui/core/Button";
import LocalMallIcon from "@material-ui/icons/LocalMall";
import BarChartIcon from "@material-ui/icons/BarChart";

const useStyles = makeStyles((theme) => ({
  submit: {
    margin: theme.spacing(2, 6),
    height: 60,
    width: 270,
    fontSize: 18,
    borderRadius: 20,
    border: "1px solid #dadce0",
  },
  fixedHeight2: {
    width: 370,
    height: 286,
    borderRadius: 40,
    border: "1px solid #dadce0",
    margin: theme.spacing(0, 6, 0),
    paddingTop: 30,
  },
  fixedHeight3: {
    width: 370,
    height: 280,
    borderRadius: 40,
    border: "1px solid #dadce0",
    margin: theme.spacing(0.2, 6, 0),
    paddingTop: 30,
  },
  fixedHeight4: {
    width: 370,
    height: 280,
    borderRadius: 40,
    border: "1px solid #dadce0",
    margin: theme.spacing(0.2, 6, 0),
    paddingTop: 30,
  },
  rightIcons: {
    fontSize: 70,
    marginLeft: "-35px",
    marginTop: theme.spacing(5),
    zIndex: 100,
    position: "absolute",
    backgroundColor: "LAVENDER",
    borderRadius: "50%",
    border: "1x solid grey",
    padding: "22px",
  },
  chart1: {
    color: "LIMEGREEN",
    margin: theme.spacing(-3, 6),
    height: 120,
    width: 270,
  },
  chart2: {
    margin: theme.spacing(-3, 6),
    height: 120,
    width: 270,
  },
}));

export default function CustomCard(props) {
  const classNamees = useStyles();
  const fixedHeightPaper2 = classNamees.fixedHeight2;
  const fixedHeightPaper3 = classNamees.fixedHeight3;
  const fixedHeightPaper4 = classNamees.fixedHeight4;
  const cardInfo = [
    {
      paper: fixedHeightPaper2,
      icon: <PeopleIcon className={classNamees.rightIcons} color="action" />,
      text: "Users Arounds",
      value: "3,456",
      value2: "+2.4",
      button: (
        <Link to="/promotProducts">
          <Button
            type="submit"
            variant="contained"
            color="secondary"
            className={classNamees.submit}
          >
            Promate Products
          </Button>
        </Link>
      ),
    },
    {
      paper: fixedHeightPaper3,
      icon: <LocalMallIcon className={classNamees.rightIcons} color="action" />,
      text: "New Orders",
      value: "124",
      value2: "+2.2",
      button: <BarChartIcon className={classNamees.chart1} />,
    },
    {
      paper: fixedHeightPaper4,
      icon: (
        <TrendingUpIcon className={classNamees.rightIcons} color="action" />
      ),
      text: "Growth",
      value: "78.7%",
      value2: "+4.2",
      button: (
        <TrendingUpIcon className={classNamees.chart2} color="secondary" />
      ),
    },
  ];
  const renderCard = (card, index) => {
    return (
      <Container key={index}>
        <Grid container>
          <Grid item xs={12} md={8} lg={9}>
            <Paper className={card.paper}>
              <Typography
                component="h1"
                variant="h4"
                align="center"
                style={{ color: "DIMGRAY", fontSize: 28 }}
              >
                {card.text}
              </Typography>
              <div style={{ display: "flex", flexDirection: "column" }}>
                {card.icon}
                <Typography
                  component="h1"
                  variant="h2"
                  style={{ color: "DIMGRAY", marginLeft: 90, marginTop: 10 }}
                >
                  {card.value}
                </Typography>
                <Typography
                  component="h4"
                  variant="h5"
                  style={{
                    color: "SPRINGGREEN",
                    marginLeft: 95,
                    marginTop: 2,
                  }}
                >
                  {card.value2}
                </Typography>
              </div>
              {card.button}
            </Paper>
          </Grid>
        </Grid>
      </Container>
    );
  };
  return <div>{cardInfo.map(renderCard)}</div>;
}
