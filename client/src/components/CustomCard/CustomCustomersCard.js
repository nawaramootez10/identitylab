import React from "react";
import { Link } from "react-router-dom";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { Typography, Paper } from "@material-ui/core";
import SupervisedUserCircleIcon from "@material-ui/icons/SupervisedUserCircle";
import { AddCustomer } from "../../components";

const useStyles = makeStyles((theme) => ({
  bigIcons: {
    margin: theme.spacing(6, 9, 4),
    fontSize: 100,
  },
  fixedHeight: {
    width: 250,
    height: 240,
    borderRadius: 20,
    border: "1px solid #dadce0",
    margin: theme.spacing(27, 2, 0),
    flex: 1,
  },
  fixedHeight1: {
    width: 250,
    height: 240,
    borderRadius: 20,
    border: "1px solid #dadce0",
    margin: theme.spacing(10, 2, 0),
    flex: 1,
  },
}));

export default function CustomPromotCard(props) {
  const classNamees = useStyles();
  const fixedHeightPaper = classNamees.fixedHeight;
  const fixedHeightPaper1 = classNamees.fixedHeight1;

  const cardInfo = [
    {
      paper: fixedHeightPaper,
    },
  ];
  const renderCard = (card, index) => {
    return (
      <Container key={index}>
        <Grid container>
          <Grid item xs={12} md={8} lg={9}>
            <Paper className={card.paper}>
              <AddCustomer />
            </Paper>
          </Grid>
        </Grid>
      </Container>
    );
  };

  const cardInfo2 = [
    {
      paper: fixedHeightPaper1,
      icon: (
        <SupervisedUserCircleIcon
          className={classNamees.bigIcons}
          color="action"
        />
      ),
      text: "Show All Customer",
    },
  ];

  const renderCard2 = (card, index) => {
    return (
      <Container key={index}>
        <Grid container>
          <Grid item xs={12} md={8} lg={9}>
            <Paper className={card.paper}>
              <Link to="/community/showCustomer">
                {card.icon}
                <Typography
                  component="h5"
                  variant="h6"
                  align="center"
                  style={{ color: "gray" }}
                >
                  {card.text}
                </Typography>
              </Link>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    );
  };

  return (
    <div>
      {cardInfo.map(renderCard)}
      {cardInfo2.map(renderCard2)}
    </div>
  );
}
