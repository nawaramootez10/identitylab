import React from "react";
import { Link } from "react-router-dom";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { Typography, Paper } from "@material-ui/core";
import StoreIcon from "@material-ui/icons/Store";

const useStyles = makeStyles((theme) => ({
  bigIcons: {
    margin: theme.spacing(5, 8.5, 4),
    fontSize: 110,
  },
  fixedHeight: {
    width: 250,
    height: 240,
    borderRadius: 20,
    border: "1px solid #dadce0",
    margin: theme.spacing(27, 2, 0),
    flex: 1,
  },
  fixedHeight: {
    width: 250,
    height: 240,
    borderRadius: 20,
    border: "1px solid #dadce0",
    margin: theme.spacing(42, 2, 0),
    flex: 1,
  },
}));

export default function CustomPromotCard(props) {
  const classNamees = useStyles();
  const fixedHeightPaper = classNamees.fixedHeight;

  const cardInfo = [
    {
      paper: fixedHeightPaper,
      icon: <StoreIcon className={classNamees.bigIcons} color="action" />,
      text: "Promote My Brands",
    },
  ];

  const renderCard = (card, index) => {
    return (
      <Container key={index}>
        <Grid container>
          <Grid item xs={12} md={8} lg={9}>
            <Paper className={card.paper}>
              <Link to="/myProducts">
                {card.icon}
                <Typography
                  component="h5"
                  variant="h6"
                  align="center"
                  style={{ color: "gray" }}
                >
                  {card.text}
                </Typography>
              </Link>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    );
  };
  return <div>{cardInfo.map(renderCard)}</div>;
}
