import React, { useState } from "react";
import { Link } from "react-router-dom";

//Mateiral UI
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { Badge, Row, Col } from "reactstrap";
import EditIcon from "@material-ui/icons/Edit";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import IconButton from "@material-ui/core/IconButton";
import VisibilityIcon from "@material-ui/icons/Visibility";
import BeenhereIcon from "@material-ui/icons/Beenhere";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { Modal, ModalHeader, ModalBody, Container } from "reactstrap";
import { Avatar } from "@material-ui/core";
import AlertComponent from "./AlertComponent";

const useStyles = makeStyles((theme) => ({
  root: {
    width: 340,
    height: 311,
    marginBottom: theme.spacing(2),
    marginRight: theme.spacing(2),
    borderRadius: 15,
    border: "1px solid #dadce0",
    cursor: "pointer",
  },
  media: {
    height: 150,
  },
}));

const ProductItem = ({ item, onDelete, onPromote }) => {
  const classes = useStyles();

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Card className={classes.root} id={item._id}>
        <CardMedia className={classes.media} title={item.productName}>
          <Avatar
            src={item.productImage}
            alt={item.productName}
            variant="square"
            style={{ height: 150, width: 340 }}
          />
        </CardMedia>
        <CardContent style={{ paddingBottom: 5, height: 111 }}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <Typography
              gutterBottom
              variant="h5"
              component="h2"
              title={item.productName}
            >
              {item.productName}
            </Typography>

            <div style={{ paddingTop: 4 }}>
              <Badge color="dark" style={{ fontSize: 15 }}>
                {item.productOnSalePrice}
                {" TND"}
              </Badge>
            </div>
          </div>
          <div
            style={{
              display: "sticky",
              flex: 1,
            }}
          >
            <Typography variant="body2" color="textSecondary" component="p">
              <b>{"Category: "}</b>
              {item.productCategory}
            </Typography>
          </div>
        </CardContent>
        <CardActions style={{ justifyContent: "space-around", padding: 0 }}>
          <Link
            to={{
              pathname: "/myProducts/editProduct",
              state: { item },
              search: "/?id=" + item._id + "?name=" + item.productName,
            }}
          >
            <IconButton title="Edit product" color="primary">
              <EditIcon />
            </IconButton>
          </Link>

          <Link
            to={{
              state: { item },
              search: "/?id=" + item._id + "?name=" + item.productName,
            }}
          >
            <IconButton title="Product details" onClick={toggle}>
              <VisibilityIcon />
            </IconButton>
          </Link>

          <AlertComponent item={item} onDelete={onDelete} />
        </CardActions>
      </Card>
      <Modal isOpen={modal} toggle={toggle} centered size="lg">
        <ModalHeader toggle={toggle}>
          {item.productName + " details:"}
        </ModalHeader>
        <ModalBody>
          <Container>
            <Row>
              <Col>
                <div style={{ boxSizing: "border-box" }}>
                  <Avatar
                    src={item.productImage}
                    alt={item.productName}
                    variant="square"
                    style={{ width: 420, height: 420 }}
                  />
                </div>
              </Col>
              <Col style={{ padding: 15, width: 420 }}>
                <Typography variant="h5" component="h2">
                  {item.productName}
                </Typography>
                <hr></hr>
                <Typography variant="body2" gutterBottom component="p">
                  <b>{"Price: "}</b> &nbsp; ${item.productInitialPrice}
                  {" - "} ${item.productOnSalePrice}
                </Typography>
                <Typography variant="body2" component="p">
                  <b>{"Description: "}</b>
                </Typography>
                <Typography
                  variant="body2"
                  gutterBottom
                  color="textSecondary"
                  component="p"
                >
                  {item.productDescription}
                </Typography>
                <Typography variant="body2" component="p">
                  <b>{"Category: "}</b>
                </Typography>
                <Typography
                  variant="body2"
                  gutterBottom
                  color="textSecondary"
                  component="p"
                >
                  {item.productCategory}
                </Typography>
                <Typography variant="body2" component="p">
                  <b>{"Tags: "}</b>
                </Typography>
                <Typography
                  variant="body2"
                  gutterBottom
                  color="textSecondary"
                  component="p"
                >
                  {item.productTags}
                </Typography>
                <Typography variant="body2" component="p">
                  <b>{"Promotion status: "}</b>
                </Typography>
                {item.status === false ? (
                  <Typography
                    variant="body2"
                    gutterBottom
                    color="textSecondary"
                    component="p"
                  >
                    <FiberManualRecordIcon style={{ color: "RED" }} />
                    Not Promoted
                  </Typography>
                ) : (
                  <Typography
                    variant="body2"
                    gutterBottom
                    color="textSecondary"
                    component="p"
                  >
                    <FiberManualRecordIcon style={{ color: "SPRINGGREEN" }} />
                    Promoted
                  </Typography>
                )}
                <br />
                <br />
                <IconButton
                  title="Promote product"
                  onClick={onPromote}
                  data-id={item._id}
                  style={{ color: "SPRINGGREEN" }}
                  disabled={item.status === true}
                >
                  <BeenhereIcon /> Promote this product
                </IconButton>
              </Col>
            </Row>
          </Container>
        </ModalBody>
      </Modal>
    </div>
  );
};

export { ProductItem };
