import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

const Spinner = () => {
  return (
    <div
      style={{
        display: "flex",
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginLeft: -40,
        marginTop: 30,
      }}
    >
      <CircularProgress size={60} />
    </div>
  );
};

export { Spinner };
