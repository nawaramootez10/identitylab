import React from "react";

//Material UI
import { ListGroupItem } from "reactstrap";
import { makeStyles } from "@material-ui/core/styles";
import { IconButton } from "@material-ui/core";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import AlertComponent from "./AlertComponent";

const useStyles = makeStyles((theme) => ({
  root: {
    height: 75,
    marginBottom: theme.spacing(1),
    borderRadius: 15,
    border: "1px solid #dadce0",
    cursor: "pointer",
  },
}));

const CustomerItemList = ({ item, onDelete }) => {
  const classes = useStyles();

  return (
    <ListGroupItem className={classes.root}>
      <div className="float-left" style={{ fontSize: 16 }}>
        <span style={{ color: "SLATEBLUE" }}>Customer Name:</span>{" "}
        <span
          style={{
            letterSpacing: 2,
            fontWeight: "bold",
          }}
        >
          {item.customerName}
        </span>
        <div>
          <span style={{ color: "SLATEBLUE" }}>E-mail:</span>{" "}
          <span style={{ letterSpacing: 2 }}>
            {item.customerEmail ? item.customerEmail : "not available email"}
          </span>
        </div>
      </div>
      <div className="float-right" style={{ fontSize: 16 }}>
        <AlertComponent item={item} onDelete={onDelete} />
      </div>
    </ListGroupItem>
  );
};

export { CustomerItemList };
