import React, { Component } from "react";
import L from "leaflet";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import "./SimpleMap.css";

import LocateControl from "./LocateControl";

var myIcon = L.icon({
  iconUrl: "https://unpkg.com/leaflet@1.6.0/dist/images/marker-icon.png",
  iconSize: [25, 41],
  iconAnchor: [13, 0],
});

class SimpleMap extends Component {
  state = {
    lat: 36.4368,
    lng: 10.677,
    zoom: 13,
  };

  render() {
    const position = [this.state.lat, this.state.lng];
    const locateOptions = {
      position: "topright",
      strings: {
        title: "Show me where I am, yo!",
      },
      onActivate: () => {}, // callback before engine starts retrieving locations
    };

    return (
      <Map className="map" center={position} zoom={this.state.zoom}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={position} icon={myIcon}>
          <Popup>
            This is my location. <br /> I'm here.
          </Popup>
        </Marker>
        <LocateControl options={locateOptions} startDirectly />
      </Map>
    );
  }
}

export default SimpleMap;
