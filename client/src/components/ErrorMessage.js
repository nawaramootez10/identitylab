import React, { Fragment } from "react";
import { connect } from "react-redux";
import { Alert } from "@material-ui/lab";

const ErrorMessageComponent = ({ message }) => {
  if (message) {
    return <Alert color="error">message</Alert>;
  }

  return <Fragment></Fragment>;
};

const mapStateToProps = ({ errors }) => {
  return {
    message: errors.message,
  };
};
const ErrorMessage = connect(mapStateToProps)(ErrorMessageComponent);
export { ErrorMessage };
