const jwt = require("jsonwebtoken");
const User = require("../models/user.model");
const jwt_decode = require("jwt-decode");

const userController = {};

userController.register = async (req, res, next) => {
  const {
    managerName,
    workspaceName,
    category,
    description,
    email,
    password,
  } = req.body;
  const newUser = new User({
    managerName,
    workspaceName,
    category,
    description,
    email,
    password,
  });

  try {
    const saved = await newUser.save();
    return res.send({
      success: true,
      user: saved,
    });
  } catch (e) {
    if (e.code === 11000 && e.name === "MongoError") {
      var error = new Error(
        "Emial address " + newUser.email + " is already taken"
      );
      next(error);
    } else {
      next(e);
    }
  }
};

userController.login = async (req, res, next) => {
  //Username, password in request
  const { email, password } = req.body;
  //Check email and password are ok
  try {
    const user = await User.findOne({ email });
    if (!user) {
      const err = new Error(
        "The email " + email + " was not found on our system"
      );
      err.status = 401;
      next(err);
    }

    user.isPasswordMatch(password, user.password, (err, matched) => {
      if (matched) {
        const secret = process.env.JWT_SECRET;
        const expire = process.env.JWT_EXPIRATION;

        const token = jwt.sign({ _id: user._id }, secret, {
          expiresIn: expire,
        });
        return res.json({
          success: true,
          token,
          decode: jwt_decode(token),
        });
      }

      res.status(401).send({
        error: "Invalid email/password combination",
      });
    });
  } catch (e) {
    next(e);
  }
};

userController.update = async (req, res, next) => {
  const user_id = req.params.user_id;
  const { managerName, workspaceName, category, description } = req.body;

  try {
    const user = await User.update(
      { _id: user_id },
      {
        managerName,
        workspaceName,
        category,
        description,
      }
    );
    return res.send({
      success: true,
      user,
    });
  } catch (e) {
    next(e);
  }
};

userController.me = (req, res, next) => {
  const { user } = req;
  res.send({ user });
};

module.exports = userController;
