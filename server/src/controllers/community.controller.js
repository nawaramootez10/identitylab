const Community = require("../models/Community.model");

const communityController = {};

communityController.create = async (req, res, next) => {
  const { customerName, customerEmail, created } = req.body;
  const newCommunity = new Community({
    customerName,
    customerEmail,
    owner: req.user,
    created,
  });

  try {
    const saved = await newCommunity.save();
    return res.send({
      success: true,
      community: saved,
    });
  } catch (e) {
    next(e);
  }
};

communityController.get = async (req, res, next) => {
  const { user } = req;

  const query = {
    owner: user._id,
  };

  try {
    const community = await Community.find(query).sort({ created: "desc" });
    return res.send({
      community,
    });
  } catch (e) {
    next(e);
  }
};

communityController.destroy = async (req, res, next) => {
  const customer_id = req.params.customer_id;

  try {
    await Community.deleteOne({ _id: customer_id });
    res.send({
      success: true,
    });
  } catch (e) {
    next(e);
  }
};

module.exports = communityController;
