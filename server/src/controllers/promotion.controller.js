const Promotion = require("../models/promotion.model");

const promotionController = {};

promotionController.get = async (req, res, next) => {
  try {
    const promotion = await Promotion.find();
    return res.send(promotion);
  } catch (e) {
    next(e);
  }
};

promotionController.create = async (req, res, next) => {
  const { startTime, endTime, promotionState, prod } = req.body;

  const newPromotion = new Promotion({
    startTime,
    endTime,
    promotionState,
    prod,
    owner: req.user,
  });

  try {
    const saved = await newPromotion.save();
    return res.send({
      success: true,
      promotion: saved,
    });
  } catch (e) {
    next(e);
  }
};

promotionController.destroy = async (req, res, next) => {
  const now = new Date();

  try {
    const check = await Promotion.findOne({ _id });
    if (
      endTime ===
      new Date(now.getFullYear(), now.getMonth, now.getDate(), now.getHours())
    ) {
      await Promotion.deleteOne({ _id });
    }
    res.send({
      success: true,
    });
  } catch (e) {
    next(e);
  }
};

module.exports = promotionController;
