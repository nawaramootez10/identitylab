const Product = require("../models/product.model");
const fs = require("fs");

const productController = {};

productController.getLatest = async (req, res, next) => {
  const { user } = req;

  const now = new Date();

  const month = parseInt(req.params.month);
  if (month && month >= 0 && month <= 11) now.setMonth(month);

  const firstDay = new Date(now.getFullYear(), now.getMonth(), 1);
  const lastDay = new Date(now.getFullYear(), now.getMonth() + 1, 0);

  const query = {
    owner: user._id,
    created: {
      $gte: firstDay,
      $lt: lastDay,
    },
  };

  try {
    const product = await Product.find(query).sort({ created: "desc" });
    return res.send({
      product,
    });
  } catch (e) {
    next(e);
  }
};

productController.get = async (req, res, next) => {
  const { user } = req;

  const query = {
    owner: user._id,
  };

  try {
    const product = await Product.find(query).sort({ created: "desc" });
    return res.send({
      product,
    });
  } catch (e) {
    next(e);
  }
};

productController.test = async (req, res, next) => {
  const { user } = req;

  const query = {
    owner: user._id,
  };

  try {
    const product = await Product.find(query).sort({ created: "desc" });
    return res.send(product);
  } catch (e) {
    next(e);
  }
};

productController.create = async (req, res, next) => {
  const {
    body: {
      productName,
      productDescription,
      productCategory,
      productTags,
      productImage,
      productInitialPrice,
      productOnSalePrice,
      productVisibility,
      created,
    },
  } = req;

  const newProduct = new Product({
    productName,
    productDescription,
    productCategory,
    productTags,
    productImage,
    productInitialPrice,
    productOnSalePrice,
    productVisibility,
    owner: req.user,
    created,
  });

  if (req.file) {
    newProduct.productImage = req.file.path;
  }

  try {
    const saved = await newProduct.save();
    return res.send({
      success: true,
      product: saved,
    });
  } catch (e) {
    next(e);
  }
};

productController.update = async (req, res, next) => {
  const product_id = req.params.product_id;
  const {
    body: {
      productName,
      productDescription,
      productCategory,
      productTags,
      productInitialPrice,
      productOnSalePrice,
      productVisibility,
    },
  } = req;

  try {
    const check = await Product.findOne({ _id: product_id });
    if (!check.owner.equals(req.user._id)) {
      const err = new Error("This product object does not belong to you!");
      err.status = 401;
      throw err;
    }

    const product = await Product.updateOne(
      { _id: product_id },
      {
        productName,
        productDescription,
        productCategory,
        productTags,
        productImage: req.file == null ? check.productImage : req.file.path,
        productInitialPrice,
        productOnSalePrice,
        productVisibility,
        created: new Date(),
      }
    );

    return res.send({
      success: true,
      product,
    });
  } catch (e) {
    next(e);
  }
};

productController.destroy = async (req, res, next) => {
  const product_id = req.params.product_id;

  try {
    const check = await Product.findOne({ _id: product_id });
    fs.unlinkSync(check.productImage, (err) => {
      console.log(err);
    });
    await Product.deleteOne({ _id: product_id });
    res.send({
      success: true,
    });
  } catch (e) {
    next(e);
  }
};

productController.promote = async (req, res, next) => {
  const product_id = req.params.product_id;
  const {
    body: { status },
  } = req;

  try {
    const check = await Product.findOne({ _id: product_id });
    if (!check.owner.equals(req.user._id)) {
      const err = new Error("This product object does not belong to you!");
      err.status = 401;
      throw err;
    }

    const product = await Product.updateOne(
      { _id: product_id },
      {
        status,
      }
    );

    return res.send({
      success: true,
      product,
    });
  } catch (e) {
    next(e);
  }
};

module.exports = productController;
