const express = require("express");
const router = express.Router();
const passport = require("passport");

const userController = require("../controllers/users.controller");
const productController = require("../controllers/product.controller");
const communityController = require("../controllers/community.controller");
const promotionController = require("../controllers/promotion.controller");
const upload = require("../middleware/upload");

// Auth and sign Up
router.post("/register", userController.register);
router.post("/login", userController.login);

// Customize and Protect the routes
router.all("*", (req, res, next) => {
  passport.authenticate("jwt", { session: false }, (err, user) => {
    if (err || !user) {
      const error = new Error("You are not authorized to access this area");
      error.status = 401;
      throw error;
    }

    req.user = user;
    return next();
  })(req, res, next);
});

// --------------- Protected Routes ----------------- //
router.get("/me", userController.me);
router.put("/register/:user_id", userController.update);
// Product routers
router.get("/product", productController.get);
router.get("/test", productController.test);
router.get("/latestproduct", productController.getLatest);
router.post(
  "/product",
  upload.single("productImage"),
  productController.create
);
router.delete("/product/:product_id", productController.destroy);
router.put(
  "/product/:product_id",
  upload.single("productImage"),
  productController.update
);
router.put("/product/promote/:product_id", productController.promote);
//Community routers
router.post("/community", communityController.create);
router.get("/community", communityController.get);
router.delete("/community/:customer_id", communityController.destroy);
//Promotion routers
router.post("/promotion", promotionController.create);
router.get("/promotion", promotionController.get);

module.exports = router;
