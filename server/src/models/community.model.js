const mongoose = require("mongoose");

const { Schema } = mongoose;

const CommunitySchema = Schema({
  customerName: { type: String, required: true },
  customerEmail: { type: String, required: true },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  created: { type: Date, default: new Date() },
});

const Community = mongoose.model("Community", CommunitySchema);
module.exports = Community;
