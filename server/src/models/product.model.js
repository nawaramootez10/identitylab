const mongoose = require("mongoose");

const { Schema } = mongoose;

const ProductSchema = Schema(
  {
    productName: { type: String, required: true },
    productDescription: { type: String, required: true },
    productCategory: { type: String, required: true },
    productTags: { type: String, required: true },
    productImage: { type: String },
    productInitialPrice: { type: Number, required: true },
    productOnSalePrice: { type: Number, required: true },
    productVisibility: { type: String, required: true },
    status: { type: Boolean, required: true, default: false },
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    created: { type: Date, default: new Date() },
  },
  { timestamps: true }
);

const Product = mongoose.model("Product", ProductSchema);
module.exports = Product;
