const mongoose = require("mongoose");

const { Schema } = mongoose;

const PromotionSchema = Schema(
  {
    startTime: { type: Date, required: true, default: new Date().getDay() },
    endTime: { type: Date, required: true },
    promotionState: { type: Boolean, required: true, default: false },
    prod: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Product",
    },
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);

const Promotion = mongoose.model("Promotion", PromotionSchema);
module.exports = Promotion;
